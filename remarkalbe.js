var $R = {}
$R.constant = {
	"ATTR_AFFILIATE" : "remarkalbe-affiliate-for",
     "ATTR_MARK" : "remarkalbe",
     "MARK_TAG" : "span",
     "MARK_SELECTOR" : "span[remarkalbe]",
	 "ID_VIEWNAV" : "remarkalbe-view-nav"
};
        
$R.language = function(){
    return "cjk";
}();
$R.parsingOptions = {
    "element_container" : ["body", "div", "article", "section", "td", "li", "dd", "dt"],
    "elements_ignore" : [
        "button", "input", "select", "textarea", "optgroup", "command", "datalist",
        "frame", "framset", "noframes",
        "style", "link", "script", "noscript",
        "canvas", "applet", "map",
        "marquee", "area", "base"
        
    ],
    "elements_ignore_tag" : [
        "form", "fieldset", "details", "dir", "center", "font"
    ],
    "elements_visible" : [
        "article", "section",
        "ul", "ol", "li", "dd",
        "table", "tr", "td",
        "div", "p", "span",
        "h1", "h2", "h3", "h4", "h5", "h6"
    ],
    "elements_self_closing" : [
        "br", "hr",
        "img",
        "col",
        "source",
        "embed", "param",
        "iframe"
    ],
	"element_copy_totally" : [
		//"blockquote", "code"
		"pre"
	],
	"elements_link_density" : [
		"div", "table", "ul", "li", "ol","section", "aside", "header" 
			],
    "elements_keep_attributes" : {
     	'a' : [ 'title', 'name'],  //href need special work
		'img' : ['width', 'height', 'alt', 'title'], //so does src
		
		'video' : ['src', 'width', 'height', 'poster', 'audio', 'preload', 'autoplay', 'loop', 'controls'],
		'audio' : ['src', 'preload', 'autoplay', 'loop', 'controls'],
		'source' : ['src', 'type'],
		
		'object' : ['data', 'type', 'width', 'height', 'classid', 'codebase', 'codetype'],
		'param' : ['name', 'value'],
		'embed' : ['src', 'type', 'width', 'height', 'flashvars', 'allowscriptaccess', 'allowfullscreen', 'bgcolor'],
		
		'iframe' : ['src', 'width', 'height', 'frameborder', 'scrolling'],
		
		'td' : ['colspan', 'rowspan'],
		'th' : ['colspan', 'rowspan']		
		//'span' : ['*']
    },
	"elements_paragraph_block": [
			"p", "div", "td", "li", "section"],
	"elements_paragraph_block_for_marks": [
			"p", "pre", "code", 'div'
	],
	"marks_ignore_in_skeleton" : ["STYLE_TAG", "STYLE_WORD"]

}
$R.domainList = {
    "domains_ignore_links" : [
    		'doubleclick.net',
			'fastclick.net',
			'adbrite.com',
			'adbureau.net',
			'admob.com',
			'bannersxchange.com',
			'buysellads.com',
			'impact-ad.jp',
			'atdmt.com',
			'advertising.com',
			'itmedia.jp',
			'microad.jp',
			'serving-sys.com',
			'adplan-ds.com'
    ],
    "domains_ignore_images" : [
   			'googlesyndication.com',
			'fastclick.net',
			'.2mdn.net',
			'de17a.com',
			'content.aimatch.com',
			'bannersxchange.com',
			'buysellads.com',
			'impact-ad.jp',
			'atdmt.com',
			'advertising.com',
			'itmedia.jp',
			'microad.jp',
			'serving-sys.com',
			'adplan-ds.com'
    ],
    "domains_keep_videos" : [
       		'youtube.com',
			'youtube-nocookie.com',
			
			'vimeo.com',
			'hulu.com',
			'yahoo.com',
			'flickr.com',
			'newsnetz.ch' 
    ]
}
var _$R_style_topic_basic = "background:#E0FA91;" //#DEFFA4;"
var _$R_style_topic_good_and_bad = "background:#E2E2E2;"
var _$R_style_topic_q_and_a = "background:#FDDFEB;"
$R.styles = {
	mark_tag :	"font-weight : bold;background-color: #9E9E9E;	margin-right: 4px;color:#fff;padding: 1px 4px;border-radius : 4px 0 0 4px;font-style: normal;",
	mark_tag_plain: "background: #fff; color:000; font-style:normal;",
	"remarkalbe-topic": _$R_style_topic_basic,
	"remarkalbe-topic-what": _$R_style_topic_basic,
	"remarkalbe-topic-how": _$R_style_topic_basic,
	"remarkalbe-topic-why":_$R_style_topic_basic,
	"remarkalbe-topic-example":_$R_style_topic_good_and_bad,
	"remarkalbe-topic-bad": _$R_style_topic_good_and_bad,
	"remarkalbe-topic-good": _$R_style_topic_good_and_bad,

	"remarkalbe-highlight" : "background:#FCF5A8;",
	"remarkalbe-bold" : "font-weight:bold;",
	"remarkalbe-underline" : "text-decoration:underline;",
	"remarkalbe-italic" : "font-style:italic;",

	"remarkalbe-question" : _$R_style_topic_q_and_a,
	"remarkalbe-answer" : _$R_style_topic_q_and_a,
	"remarkalbe-word" : "background:#B9DFF1;",
	"remarkalbe-tag" : "background:#FF5918; color:#fff;",
	"remarkalbe-tag-section-tag" : "background:#FF5918; color:#fff; border-radius:4px; padding:1px 4px;margin-left:5px;"

}
$R.textHelper = {}
$R.textHelper.getTextLength = function(_text){
    return _text.replace(/[\r\n\s]/gi, "").length
}
$R.textHelper.getWordCount = function(_text){
    
    var _gbk_char =  [
        [/[\u3002]/gi, 4],
        [/[\uff1b|\uff1a|\u201c|\u201d|\uff08|\uff09|\u3001|\u300a|\u300b||\uff0c|\uff1f]/gi, 1]
    ];
    _text = _text.replace(/[\r\n\s]+/gi, ' ');
    _text = _text.replace(/([.,?!:;()\[\]'""-])/gi, '$1 ');
    for (var i in _gbk_char){
        _text = _text.replace(_gbk_char[i][0], '[=word('+_gbk_char[i][1]+')]');
    }
    var _count = 0,
        _words_count = _text.match(/([^\s\d]{3,})/gi);
    _count += (_words_count !=null ? _words_count.length : 0);
    
    _text.replace(/\[=word\((\d)\)\]/, function(_match, _num){
        _count += (5 * parseInt(_num))
    })
     
    return _count;
}
$R.imageHelper = {}
$R.imageHelper.getImageType =function(_image){
    var _width = $(_image).width(),
        _height = $(_image).height();
    switch (true){
    case (_width * _height >= 50000):
    case (_width >= 400 && _height >= 80):
        return "large";
    case (_width * _height >= 20000):
    case (_width >= 150 && _height >= 150):
        return "medium";
    case (_width <= 5 && _height <= 5):
        return "skip";
    default:
        return "small";
    }
} 

$R.log = {}
$R.log.warn = function(msg){
    console.log(msg)
}
$R.log.info = function(msg){
    console.log(msg)
}
$R.commonHelper = {}
$R.commonHelper.getOuterHTML = function(_nodes){
	var _tmp = $("<div>");
    var _recursion = function(_container){
        var _parent = document.createElement(_container["tag"])
        var _nodes = _container["children"]
        for (var i  in _nodes){
            if (_nodes[i].tag) {
                _parent.appendChild(_recursion(_nodes[i]));
            }else{
                $(_parent).append($(_nodes[i]).clone());
            }
        } 
        return _parent;
    };
    _tmp.append(_recursion(_nodes))
    $(_tmp).find(".remarkalbe-end").after("<br />")
	return _tmp.html();
	
}
$R.commonHelper.pluralize = function (a) {
    return a + 's';
}
$R.commonHelper.candidateSort = function(field, max, second_field, second_max) {
    max = max ? -1 : 1
	var _compare =  function(a, b){
		var _a = a, _b = b;
		if (field.constructor == String){
			field = [field];
		};
		for (var i = 0; i <field.length; i++) {
		_a = _a[field[i]]
		_b = _b[field[i]]
		};
        if (_a < _b){
            return -max;
        }else{
            if (_a > _b){
                return max;
		}else{
				if (second_field) return $R.commonHelper.candidateSort(second_field, second_max)(a, b);
                return 0;
            }
        }
	}
	return _compare;

}
var inList = function (_tag, _category, _tag_name){
    var _result = $R.parsingOptions[_category]
    if (_tag_name){
        _result = _result[_tag_name];
    }
    return _result.indexOf && (_result.indexOf('*') == 0 || _result.indexOf(_tag) > -1);
}

var containsPart = function(_href, _list){
    for (var i = 0, _len = $R.domainList[_list]; i < _len; i++){
        if (_href.indexOf($R.domainList[_list][i] > -1)){
            return true
        }
    }
}
var zeroDimension = function (_node){
    return !(_node.offsetWidth + _node.offsetHeight + _node.offsetLeft + _node.offsetTop);
}
$R.exploreNode = function(nodeToExplore, _marked, _just_explore){
    var 
    element_index = 0,
    inside_link = false,
    inside_link_element_index = 0,
    
    length_above_plain_text = 0,
    count_above_plain_words = 0,
    length_above_links_text = 0,
    count_above_links_words = 0,
    count_above_candidates = 0,
    count_above_containers = 0,
    
    above_plain_text = "",
    above_links_text = "",
    
    result_containers = [],
    result_candidates = [],
    result_links = [],
	result_paragraphs = [];
    var isGoodCandidate = function (_result){
        switch (true){
        case ($R.language != "cjk" 
                && _result.count_links * 2 >= _result.count_words.plain_words):
        case ($R.language != "cjk"
                && _result.length_text.plain_text * 3 < 65):
        case ($R.language != "cjk"
                && _result.count_words.plain_words < 5):
        case ($R.language == "cjk"
                && _result.length_text.plain_text < 10):
        case ($R.language == "cjk"
                && _result.count_words.plain_words < 2):
                return false;
        default:
                return true;
        }
    }
    var recursion_explore = function(_node){
        element_index++;
        var tag_name = "#valid"
        switch (_node.nodeType)
        {
            case 3: tag_name = "#text"; break;
            case 1: if (_node.tagName && _node.tagName >"") tag_name = _node.tagName.toLowerCase();break
        }
        var result = {
            "index" : element_index,
            "node" : _node,
            
            "is_container" : ($R.parsingOptions.element_container.indexOf(tag_name) > -1),
           	"is_paragraph" : ($R.parsingOptions.elements_paragraph_block.indexOf(tag_name) > -1),
            "is_candidate" : false,
            "is_text" : false,
            "is_link" : false,
            "is_link_skip" : false,
			
			"is_mark" : false,
            
            
            "is_image_small" : false,
			"is_image_medium" : false,
			"is_image_large" : false,
            "is_image_skip" : false,
            
            "image_type" : "",
            
            "length_text" : {
                // above 
                "above_plain_text" : length_above_plain_text,
                "above_links_text" : length_above_links_text,
        
                "above_all_text" : length_above_links_text + length_above_plain_text,
                
                 //contains
                "plain_text" : 0,
                "links_text" : 0,
                
                "all_text" : 0

             },   
            "count_words" : {
                "above_plain_words" : count_above_plain_words,
                "above_links_words" : count_above_links_words,
                "above_all_words" : count_above_links_words + count_above_plain_words,
                
                "plain_words" : 0,
                "links_words" : 0,
                
                "all_words" : 0
            },
            "count_containers" : {
                "above_containers" : count_above_containers,
                "containers" : 0
            },
            "count_candidates" : {
                "above_candidates" : count_above_candidates,
                "candidates" : 0
            },
            "count_links" : {
                "links" : 0,
                "links_skip" : 0
            },          
            "count_images" : {
                "images_small" : 0,
                "images_medium" : 0,
                "images_large" : 0,
                "images_skip" : 0
            },
			"count_marks" : {
					"marks" : 0
			},
			"count_children" : {
				"contains_mark" : 0,
				"mark_coverage" : 0
			}
            
    }
        
        //node ignored
        if (tag_name == "#invalid"
            || inList(tag_name, "elements_ignore"))
            return;
        
        //todo: here should consider inline div with no w/h
        if (inList(tag_name, "elements_visible") && zeroDimension(_node))
            return;
            
        if (inList(tag_name, "elements_self_closing") && tag_name != "img")
            return;
        switch (tag_name)
        {
            case "#text":
                result.is_text = true;
                var _node_text = _node.nodeValue;
                
                var _length_plain_text, _count_plain_words;
                _length_plain_text = result["length_text"]["plain_text"] = $R.textHelper.getTextLength(_node_text);
                _count_plain_words = result["count_words"]["plain_words"] = $R.textHelper.getWordCount(_node_text);
                
                if (inside_link){
                    length_above_links_text += _length_plain_text;
                    count_above_links_words += _count_plain_words;
                }else{
                    length_above_plain_text +=_length_plain_text;
                    count_above_plain_words += _count_plain_words;
                }
                return result;
            case "a":
                var _href = _node.href;
                if ( _href == "" )
                    break;
                 
                result.is_link = true;
                 
                if (containsPart(_href, "domains_ignore_links")){
                    result.is_link_skip = true
                }
                
                if (inside_link);
                else{
                    inside_link = true;
                    inside_link_element_index = result.index;
                }
                
                result_links.push(result)
                break;
                
            case "img":
                var _src = _node.src;
                result.image_type = $R.imageHelper.getImageType(_node);
                if (_src && containsPart(_src, "domains_ignore_images")){
                    result.image_type = "skip"
                }
                result["is_image_" + result.image_type] = true;
                break;
			
			case $R.constant['MARK_TAG']:
				if (!_marked){
					break;
				}else{
					if ($M.isMark(_node)){
						result.is_mark = true;
					}
				}
        }
        for (var i = 0, _len = _node.childNodes.length; i < _len; i++){
            var _child = _node.childNodes[i];
            if (_child.innerHTML && _child.innerHTML.length > 10)
            {
                //$R.log.info(_child.innerHTML)
            }
            var _child_result = recursion_explore(_child);
            if (!_child_result){
                continue;
            }
            
            var _count_fields = ["link", "image", "container", "candidate", "mark"]
            for (var f in _count_fields){
                var _category = $R.commonHelper.pluralize(_count_fields[f])
                _count_category = "count_" + _category
                var _fields = result[_count_category]
                for (var j in _fields){
                    //singularize the is_xx
                    //$R.log.info("is_" + j.replace(_category, _count_fields[f]))
                    //$R.log.info(_child_result)
                    var _flag = _child_result["is_" + j.replace(_category, _count_fields[f])];
                    if (_flag == undefined )
                    {
                        //$R.log.warn("couldn't get count field " +_count_category + ":" + j )
                        continue;
                    }
                   
                    result[_count_category][j] += _child_result[_count_category][j];
                    result[_count_category][j] += _flag ? 1 : 0;
                }
            }
			if (_child_result.is_mark || _child_result.count_marks.marks > 0) {
					result["count_children"]["contains_mark"] +=1;
					result["count_children"]["mark_coverage"] += _child_result["length_text"]["all_text"];
			}	
            if (_child_result.is_link)
            {
               
                result["length_text"]["links_text"] += _child_result["length_text"]["plain_text"] + _child_result["length_text"]["links_text"];
                result["count_words"]["links_words"] += _child_result["count_words"]["plain_words"] + _child_result["count_words"]["links_words"]
            }else{
                result["length_text"]["plain_text"] += _child_result.length_text.plain_text;
                result.count_words.plain_words += _child_result.count_words.plain_words;
                result.length_text.links_text += _child_result.length_text.links_text;
                result.count_words.links_words += _child_result.count_words.links_words;
            }
                       
        }
        result["length_text"]["all_text"] += result["length_text"]["plain_text"] + result.length_text.links_text;
        result["count_words"]["all_words"] += result.count_words.plain_words + result.count_words.links_words;
		result["count_children"]["mark_coverage"] /= result["length_text"]["all_text"];
		
        if ((result.is_link)
                &&(inside_link_element_index == result.index)){
                    inside_link = false;
                    inside_link_element_index = 0;
                }
		if (result.is_paragraph) result_paragraphs.push(result);
        if (result.is_container
            || result.index == 1 || _just_explore){
                result_containers.push(result);
                if (result.is_container){
                    count_above_containers ++;
                }
                if (!_just_explore && isGoodCandidate(result)){
                    result.is_candidate = true;
                    result_candidates.push(result);
                    
                    count_above_candidates++ ;
                }
            }
        //$R.log.info(result)
        return result
    }

    recursion_explore(nodeToExplore)
	if (_just_explore) return result_containers.pop();
    return {
        "containers" : result_containers,
        "candidates" : result_candidates,
		"links" : result_links,
		"paragraphs" : result_paragraphs
    }
}

$R.processCandidates = function(candidates){
    candidates.sort($R.commonHelper.candidateSort("index"))
    var _main = candidates[0];
    for (var i = 0, _len = candidates.length; i < _len; i++){
        var _count_pieces = 0
        for (var j = i; j < _len; j++){
            if (candidates[j].count_candidates.candidates == 0
                && jQuery.contains(candidates[i].node, candidates[j].node))
                _count_pieces++;
        }
        candidates[i]["count_pieces"] = {"pieces" : _count_pieces};
        candidates[i]["assessment"] = $R.computeFurtherDetails(candidates[i], candidates[0])
    }
    return candidates
}

$R.computeFurtherDetails = function(candidate, total){
    var result = {
        "paragraphs" : {},
        "words" :{},
        "text" : {},
        "links" : {},
        "above_text" : {},
        "candidates" : {},
        "containers" : {},
        "pieces" : {},
		
		"marks" : {}
    }
    _total = jQuery.extend({}, total)
    var calculateRatio = function(_first, _category, _field, _second, _second_category, _second_field){
        var _unit_name =  _category.split("_")[0],
            _result_category = _category.split("_")[1],
            
            _second_field = _second_field ? _second_field : (_second_category ? _second_category : _field),
            
            _second_category = _second_category ? (_second_category == _second_field ? _category : _second_category) : _category,
            _is_total = _second == _total ? true : false,
            _ratio_name = ["ratio", _unit_name, _field, _is_total? "to_total" : "to",  _second_field ].join("_");
        //$R.log.info(_category + ":" + _field )
        //$R.log.info(_second_category + ":" + _second_field)
        var
            a = _first[_category][_field],
            b = _second[_second_category][_second_field];
        
        //$R.log.info(_result_category)
        //$R.log.info(result[_result_category])
        //$R.log.info(_ratio_name)
        result[_result_category][_ratio_name] = a / (b == 0 ? 1 : b) ;
    }
    //paragraphs
    result.paragraphs.count_lines = candidate.length_text.plain_text / 65;
    result.paragraphs.count_paragraphs_3_lines = result.paragraphs.count_lines / 3;
    result.paragraphs.count_paragraphs_5_lines = result.paragraphs.count_lines / 5;
    
    result.paragraphs.count_paragraphs_50_words = candidate.count_words.plain_words / 50;
    result.paragraphs.count_paragraphs_80_words = candidate.count_words.plain_words / 80;
    
    //text   
    calculateRatio(candidate,"length_text", "plain_text", _total);
    calculateRatio(candidate,"count_words","plain_words", _total);
    
    //links
    calculateRatio(candidate,"length_text", "links_text", candidate, "plain_text");
    calculateRatio(candidate,"count_words", "links_words", candidate, "plain_words");
    
    calculateRatio(candidate,"length_text", "links_text", candidate, "all_text");
    calculateRatio(candidate,"count_words", "links_words", candidate, "all_words");
    
    
    calculateRatio(candidate,"length_text", "links_text", _total);
    calculateRatio(candidate,"count_words","links_words", _total);
    
    calculateRatio(candidate, "count_links", "links", _total);
    calculateRatio(candidate, "count_links", "links", candidate, "count_words", "plain_words");
    
    //text above
    calculateRatio(candidate, "length_text", "above_plain_text", _total, "plain_text");
    calculateRatio(candidate, "count_words", "above_plain_words", _total, "plain_words");
    
    //candidates
    calculateRatio(candidate, "count_candidates", "candidates", _total);
    
    calculateRatio(candidate,"count_containers", "containers", _total);
    
    //pieces
    calculateRatio(candidate,"count_pieces", "pieces", _total);
	
	//marks
	calculateRatio(candidate, "count_marks", "marks", _total);
    return result;
}

$R.calculateScore = function(_factor, _power, _value, _base){
    var _remains = _base * (1 - _factor)
    if (_value < 0)
    {
        return _remains;
    }else{
        return _remains + _base * _factor * Math.pow(_value, _power);
    }
}

$R.calculateScoreHistory = function(_parmas, base){
    var _s = base,
        _history = [_s]
    for (var i in _parmas){
        p = _parmas[i];
        _s = $R.calculateScore(p.factor, p.power, p.value, _history[0])
        _history.unshift(_s)
    }
    return _history;
}

$R.computeScoreForCandidate = function(_candidate, _marked){
    var _assessment = _candidate.assessment;
        base = _assessment.paragraphs.count_paragraphs_3_lines
            + _assessment.paragraphs.count_paragraphs_5_lines * 1.5
            + _assessment.paragraphs.count_paragraphs_50_words
            + _assessment.paragraphs.count_paragraphs_80_words * 1.5
            + _candidate.count_images.images_large * 3
            - _candidate.count_images.images_skip * 0.50
            - _candidate.count_images.images_small * 0.50
    
	if (_marked) base += _candidate.count_marks.marks;
    var _factors;
    _factors = [
        //	total text
        {   value : _assessment["text"]["ratio_length_plain_text_to_total_plain_text"],
            factor : 0.5,
            power : 2
        },
        {   value : _assessment["words"]["ratio_count_plain_words_to_total_plain_words"],
            factor : 0.5,
            power : 2
        },
        
        //text above
        {   value : 1 - _assessment["text"]["ratio_length_above_plain_text_to_total_plain_text"],
            factor : 0.5,
            power : 5
        },
        {   value : 1 - _assessment["words"]["ratio_count_above_plain_words_to_total_plain_words"],
            factor : 0.5,
            power : 5
        
        },
        
        //links outer
        {   value : 1 - _assessment["text"]["ratio_length_links_text_to_total_links_text"],
            factor :  0.5,
            power : 1
        },
        {   value : 1 - _assessment["words"]["ratio_count_links_words_to_total_links_words"],
            factor : 0.5,
            power : 1
        },
        {   value : 1 - _assessment["links"]["ratio_count_links_to_total_links"],
            factor : 0.5,
            power : 1
        },
        
        //links inner
        {   value : 1 - _assessment["text"]["ratio_length_links_text_to_plain_text"],
            factor : 0.5,
            power : 1
        },
        {   value : 1 - _assessment["words"]["ratio_count_links_words_to_plain_words"],
            factor : 0.5,
            power : 1
        },
        {   value : 1 - _assessment["text"]["ratio_length_links_text_to_all_text"],
            factor : 0.5,
            power : 1
        },
        {   value : 1 - _assessment["words"]["ratio_count_links_words_to_all_words"],
            factor : 0.5,
            power : 1
        },
        {   value : 1 - _assessment["links"]["ratio_count_links_to_plain_words"],
            factor : 0.5,
            power : 1
        },
        
        //candidates
        {   value : 1 - _assessment["candidates"]["ratio_count_candidates_to_total_candidates"],
            factor : 0.5,
            power : 1
        },
        
        //containers
        {   value : 1 - _assessment["containers"]["ratio_count_containers_to_total_containers"],
            factor : 0.5,
            power : 1
        },
        
        //pieces
        {   value : 1 - _assessment["pieces"]["ratio_count_pieces_to_total_pieces"],
            factor : 0.5,
            power : 1
        }
        
        ];
		
	if (_marked){
		_factors.push({
			value : _assessment["marks"]["ratio_count_marks_to_total_marks"],
			factor : 0.5,
			power : 2
		})
	}
    //$R.log.info(_factors)
    return $R.calculateScoreHistory(_factors, base * 1000)
}
$R.judgeNode = function(_node){
    var _tag_name = "#valid"
    switch (_node.nodeType)
    {
        case 3: _tag_name = "#text"; break;
        case 1: if (_node.tagName && _node.tagName >"") _tag_name = _node.tagName.toLowerCase();
            break;
    }
    result = {type : "valid", tag : _tag_name, ignore : true}
    if (_tag_name == "#valid")
        return result
    if (inList(_tag_name, "elements_ignore")){
        result['type'] = "elements_ignore"
        return result;
    }
    if (_tag_name == "#text"){
        result['type'] = 'text'
        result['ignore'] = false
        return result;
    }
    if (inList(_tag_name, "elements_visible")
        && zeroDimension(_node)){
        result['type'] = "elements_invisible";
        return result;
    }
    switch (_tag_name){
    case 'object':
    case 'embed':
    case 'iframe':
        var _src = (_tag_name == "object" ? $(_node).find("param[name='movie']").attr('value') : $(_node).attr('src')),
        _skip = (_src > "" ? false : true)
        
        if (!containsPart(_src, "domains_keep_videos"))
        {
            _skip = true; 
        }
        if (_skip){
            result['type'] = 'video_skip'
            return result
        }
    }
    if (_tag_name == 'a' && containsPart(_node.href, "domains_ignore_links"))
    {
        result['type'] = "links_ignore"
        return result;
    }
	var _assess = false;
	//skip_link
	if (_tag_name == 'a' || _tag_name == 'li'){
			_assess = $R.exploreNode(_node, true, true);
			if (_assess.count_images.images_small + _assess.count_images.images_skip > 0){
				return result;
			}
	}
	if (inList(_tag_name, "elements_link_density") && (_node.innerText || _node.textContent).length < 65 * 2){
			_assess = _assess || $R.exploreNode(_node, true, true);
			if	(_assess.count_links.links >= 2 
					&& _assess.count_marks.marks == 0
					//&& _assess.count_words.links_words > _assess.count_words.plain_words 
					&& _assess.length_text.links_text > _assess.length_text.plain_text
					&& (_assess.count_images.images_large + _assess.count_images.images_medium == 0)){
							return result;

			}
			if ((_node.innerText || _node.textContent).length == 0) return result;

	}
	if (_tag_name == "img"){
		_assess = _assess || $R.exploreNode(_node, true, true);
		if (_assess.is_image_skip) return result;
	}
    result['ignore'] = false;
    return result
    
}
$R.generateMarkTag = function(_node){
		var _t = $M.styleTag[$M.getMarkType(_node)]
		var _style = $R.styles['mark_tag']
		var _content_html = ""
		if (_t){
				_content_html += "<i class='remarkalbe-mark-tag'" + " style=\"" + _style + "\">"
				_content_html += _t
				_content_html += "</i>"
		}
		return _content_html
}
$R.insertMarkTag = function(_node){
	$(_node).prepend($R.generateMarkTag(_node))
}
$R.getMarkCSSStyle = function(_node){
	/*
	var _attrs = ["background", "color", "font-weight", "text-decoration", "margin", "padding", "font-style", "border-radius"]
	var _result = {}
	for (var i = 0; i < _attrs.length; i++) {
		_result[_attrs[i]] = $(_node).css(_attrs[i])
	};
	_result.toString = function(){
		var _str = []
		for (var i in this){
			if (i == 'toString') continue;
			_str.push([i, this[i]].join(":"))
		}
		return _str.join(";") 
}
*/
	var _mark_type = $M.getMarkType(_node)//$(_node).attr($M.constant["ATTR_MARK_TYPE"])
	return $R.styles[_mark_type]
}
$R.purifyContent = function(_node){
    var  addAttribute = function (_attr, _value){
        return _value >"" ? ' '+ _attr + '="' + _value + '"' : '';
    }
    var _content_html = "";
    var _recursion = function(_node){
        var _judge_result = $R.judgeNode(_node),
            _tag_name = _judge_result["tag"];
        if (_judge_result['type'] == 'text'){
            _content_html += _node.nodeValue.replace(/</gi, '&lt;').replace(/>/gi, '&gt;');
            return;
		}
		var _is_mark = $M.isMark(_node); 
		if (_judge_result["ignore"]){
			if (!_is_mark){
				return;
			}
		}
        //start tag
		
        if (!inList(_tag_name, "elements_ignore_tag")){
            _content_html += '<' + _tag_name;
            
            //attributes
            _attrs = _node.attributes;
			if (_is_mark){
				for (var i = 0, _len = _attrs.length; i< _len; i++){
                    _attr = _attrs.item(i).name;
					_value = _node.getAttribute(_attr)
					_content_html += addAttribute(_attr, _value)
                }
				_content_html += " style='" + $R.getMarkCSSStyle(_node) + "'"
	
			}
            if (!_is_mark && _attrs && _tag_name in $R.parsingOptions.elements_keep_attributes){
                for (var i = 0, _len = _attrs.length; i< _len; i++){
                    _attr = _attrs.item(i).name;
                    if (inList(_attr, "elements_keep_attributes", _tag_name)){
                        _value = _node.getAttribute(_attr)
                        
                        _content_html += addAttribute(_attr, _value)
                    }
                }
            }
            
            _content_html += addAttribute('id', _node.getAttribute('id'))
            
			switch (_tag_name){
				case 'a':
                _content_html += addAttribute('target', '_blank');
				_content_html += " "
                _content_html += addAttribute('href', _node.href);
				break;
				case 'img':
				_content_html += " "
				_content_html += addAttribute('src', _node.src);
				break;
			}
            if (inList(_tag_name, "elements_self_closing")){
                _content_html += ' />'
                return;
            }else{
                _content_html += '>';
            }
            
            
        }
        
        if ($M.isMark(_node, "MARK_START")){
        	_content_html += $R.generateMarkTag(_node)
        } 
        //childNodes
		if (false){//inList(_tag_name, "element_copy_totally")){
			_content_html += $(_node).html();//replace(/</gi, '&lt;').replace(/>/gi, '&gt;');
		}else{
			for (var i = 0, _len = _node.childNodes.length; i < _len; i++)
				_recursion(_node.childNodes[i]);
		}
        
        //end tag
        if (inList(_tag_name, "elements_ignore_tag"))
            return;
        _content_html += '</' + _tag_name + '>';
    }
    
    _recursion(_node)
    return _content_html
}
$R.getMainContent = function(_body, _marked){
    var _stuff = $R.exploreNode(_body, _marked)
    var candidates = $R.processCandidates(_stuff.candidates)
    //$R.log.info(candidates)
    for (i in candidates){
        candidates[i].scores = $R.computeScoreForCandidate(candidates[i], _marked)
        candidates[i].score = candidates[i].scores[0]
    }
    candidates.sort($R.commonHelper.candidateSort("score", true))
    _content_html = $R.purifyContent(candidates[0].node)

    return { "candidates" : candidates, "content" : _content_html}
}
$R.getPlainText = function(_fragment){
    return _fragment.replace(/<[^>]+?>/gi, "").replace(/\s+/gi, " ").replace(/[\n\r]+/, "").trim();
}
$R.isGoodTitle = function(_title, _document_title){
    var _plain_title = $R.getPlainText(_title);
    var _title_length = $R.textHelper.getTextLength(_plain_title);
	if (_title_length < 5 || _title_length > 65 * 3) return 0;
    if (_document_title.indexOf(_plain_title) > -1){
        return _plain_title
    }
    return 0;
    
}
$R.getTitleAndPostionFromFragment = function(_content_html, _document_title){
	var _heads_regexs = [
		/<(h1)[^>]*>([\s\S]+?)<\/\1>/gi,
		/<(h2)[^>]*>([\s\S]+?)<\/\1>/gi,
		/<(h3|h4|h5|h6)[^>]*>([\s\S]+?)<\/\1>/gi
	];
	for (var i = 0, _len = _heads_regexs.length; i < _len; i++){
		var _match = _heads_regexs[i].exec(_content_html)
		if (!(_match && _heads_regexs[i].lastIndex > -1)){
			continue;
		}
		var _end_pos = _heads_regexs[i].lastIndex,
			_start_pos = _end_pos - _match[0].length,
			_head_type = _match[0],
			_head_content = _match[2];
			_good_title = $R.isGoodTitle(_head_content, _document_title)
		if (_good_title){
			return	[_good_title, _end_pos, _start_pos]
		}
	}
	return [0, 0]
}
$R.getTitleFromFragment = function(_content_html, _document_title){
	return $R.getTitleAndPostionFromFragment(_content_html, _document_title)[0]
}
$R.generateTitleHeader = function(_title){
	var _start = "<div class='remarkalbe-title-header'><h1>",
		_end = "</h1></div>";
	return _start + _title + _end;
}
$R.generateContentSection = function(_content){
	return "<div class='remarkalbe-content-section'>" + _content + "</div>"
}
$R.generateSourceURLSection = function(){
	var _url = window.location.href;
	return $("<div class='remarkalbe-source-url-section'  style='font-style:italic;color: #999;'>source page &lt;</div>").append($("<a>", {href:_url, target: "_blank", style: "color:#999"}).text(_url)).append("&gt;")[0].outerHTML;
}
$R.generateTagSection  = function(_tags){
		if (!_tags| _tags.length ==0 ) return "";
		var _results = $.map(_tags, function(_tag){
				return ["<span class='remarkalbe-tag-section-tag' style='", $R.styles["remarkalbe-tag-section-tag"],"'>", _tag, "</span>"].join("");
			});
		return ["<div class='remarkalbe-tag-section' style='margin-bottom:10px;'>", "<span>tags: </span>", _results.join(""), "</div>"].join("");
}
$R.generateWordSection = function(_words){
		if (!_words | _words.length ==0 ) return "";
		var _results = $.map(_words, function(_word){
				return ["<div  class='remarkalbe-word-section-word'>", _word, "</div>"].join("");
			});
		return ["<div class='remarkalbe-word-section'><h3 style='color:#00A3F0;font-weight:bold;display:block !important;margin:6px 0;text-decoration: underline;font-style:italic;'>Words List</h3>", _results.join(""), "</div>"].join("");
}
$R.getTitleAndSplit = function(_candidate, _content_html,_page){
	 var _document_title = _page.document.title > '' ? _page.document.title : '';
	//already constains?
	var _title_pos_result = $R.getTitleAndPostionFromFragment(_content_html, _document_title),
		_title = _title_pos_result[0],
		_end_pos = _title_pos_result[1];
		_start_pos = _title_pos_result[2];
	
	//above
	var _current_node = _candidate.node;
	var _above_html = ""
	var _title_mark = ""
	if (!_title){
			do{
					if (_current_node.tagName && _current_node.tagName.toLowerCase() == "body") break;
					if (_current_node.previousSibling)
					{
							_current_node = _current_node.previousSibling;
					}else{
							_current_node = _current_node.parentNode;
							continue;
					}
					_above_html = $R.purifyContent(_current_node) + _above_html;
					_title = $R.getTitleFromFragment(_above_html, _document_title)
					if ($R.textHelper.getTextLength($R.getPlainText(_above_html)) > 65 * 3 * 3) break;
			} while (true && !_title)
			//if (_title) _title_mark = $R.findMarkInNode(_current_node)
	}
	//title
	if (_title){
		_content_html  = _above_html + _content_html; 
		_title_pos_result = $R.getTitleAndPostionFromFragment(_content_html, _document_title);
		_title = _title_pos_result[0];
		_end_pos = _title_pos_result[1];
		_start_pos = _title_pos_result[2];
	}
	if (!_title){
		var _title_split = /<<|>>|\||-/g;
		var _parts = _document_title.split(_title_split);
		if (_parts.length > 1){
			//_parts.sort($R.commonHelper.candidateSort("length", true))
			if (_parts[0].length > _document_title.length * 0.3) _title = _parts[0];
		}
		//finally
		if (!_title) _title = _document_title;
		_current_node = _candidate.node
	}
	return { title : _title , before_node : _current_node, content : _title_mark + _content_html.substr(0, _start_pos || 0) + _content_html.substr(_end_pos) }
	
}
$R.addMarkNotIncluded = function(_content, _body){
	var _mark_selector = $R.constant["MARK_SELECTOR"],
		$content = $(_content),
	    _all = $(_body).find(_mark_selector).toArray(),
	    _already = $content.find(_mark_selector).toArray(),
		_get_mark_id = function(_marks){
			var _result = {}
			for (var i = 0; i < _marks.length; i++) {
				_result[$(_marks[i]).attr("remarkalbe")] = _marks[i];
			}
			return _result;
		},
		_all_dict = _get_mark_id(_all),
		_already_dict = _get_mark_id(_already),
		_before = (function(){
			var _result = []
			for (var i = 0; i < _all_dict.length; i++) {
				if (i in _already_dict) break;
				_result.push(_all_dict[i]);
			}
			return _result;
		}()),
		_after = (function(){
			var _result = []
			for (var i = 0; i < _all_dict.length; i++) {
				if (i in _already_dict || i in _before) continue;
				_result.push(_all_dict[i]);
			}
			return _result;
		}()),
		_addMarks = function(_marks, _result){
			var _temp = $("<div>")
			for (var i = 0; i < _marks.length; i++) {
				_temp.append($R.insertMarkTag($(_marks[i]).clone()));
			}
			_result.push(_temp.html())
		};
	_result = [];
	_addMarks(_before, _result);
	_result.push(_content);
	_addMarks(_after, _result);
	return _result.join("");
}
$R.addMarkTag = function(_content){
    var $content = $(_content)
    var starts = $content.find(".remarkalbe-start")
    starts.each(function(){
        var s = this;
        $(s).prepend("<i>hello</i>")
    });
    return $content.html()
}
$R.getSkeleton = function(_content){
	var _head = "h1,h2,h3,h4,h5,h6",
		_count = {
			"marks" : 0,
			"style_marks" : 0,
			"homeless_style_marks" : 0, 
			"structure_marks" : 0,
			"heads" : 0
		},
		_is_node_ignore = function(_node){
			return $(_node).find($R.constant["MARK_SELECTOR"]).length == 0 && $(_node).find(_head).length == 0
		},
		_result = { tag : "div", children : []},
		_selector = [$M.selectors.byStyle("STYLE_STRUCTURE"), $M.selectors.byStyle("STYLE_UTILITY")].join(","),  
		_get_clear_mark = function(_node){
			// remove marks except keypoint marks.  
			// how about words? words has not utility class
			var _dup = $(_node).clone()

			//remove prefix tag
			//remarkalbe-mark-tag' 
			//_dup.find(".remarkalbe-mark-tag").remove()
			_dup.find(_selector).each(function(){
				var item = this;
				$(item).find(".remarkalbe-mark-tag").remove() 
				$(item).after($(item).html());
				//_dup[0].removeChild(item)
				$(item).remove()
			})

			if ($M.is_pdf){
				_dup.find('div').each(function(){
					var item = this;
					item.outerHTML = '<span>' + $(item).html() + '</span>';
				})
			}
			return _dup[0];
		},
		_recursion = function(_node, _container,_in_mark){
			var _children_in_mark = false;
			var _tag_name = _node.tagName && _node.tagName.toLowerCase();
			switch (true){
				case _node.tagName && _head.indexOf(_tag_name) >= 0:
				case (!_in_mark) && $M.isMark(_node):
				case _in_mark && ( $M.isMark(_node, "STYLE_UTILITY") || $M.isMark(_node, "STYLE_STRUCTURE")):
					_container["children"].push(_get_clear_mark(_node));
					_children_in_mark = true
					break;
			}
			if (_is_node_ignore(_node)) return;
			if (!(_node.childNodes)) return;
            if (inList(_tag_name, "elements_paragraph_block_for_marks") && !($M.is_pdf && _tag_name == 'div')){ 
                var _paragraph = { tag : _tag_name, children : []};
                _container["children"].push(_paragraph);
            }else{
                var _paragraph = _container;
            }
			for (var i = 0, _len = _node.childNodes.length; i < _len; i++){
				_recursion(_node.childNodes[i], _paragraph, _children_in_mark);
			}
        };
    _recursion(_content, _result);
	console.log(_result);
	return $R.generateHTMLForSkeleton(_result);
}
$R.generateHTMLForSkeleton = function(_nodes){
	var _tmp = $("<div>"),
		_marks_ignore = $.map($R.parsingOptions["marks_ignore_in_skeleton"], function(_item){ return $M.getStyle(_item)}),
		_is_mark_ignore = function(_node){
			return _marks_ignore.indexOf($M.getMarkType(_node)) >=0 
		},
		_is_container_wrap = function(_node){
			switch (_node.tag){
			case "pre":
			case "code":
				return _node.tag;
			}
		}

	var _total = null;
    var _recursion = function(_container, _add_wrap, _last){
        var _parent = document.createElement(_container["tag"])
        var _nodes = _container["children"]
		_last.appendChild(_parent)
		if (!_total) _total = _last;
        for (var i  in _nodes){
            var _node = _nodes[i]
            if (_node.tag) {
                //_parent.appendChild(_recursion(_node, _is_container_wrap(_node)));
				_recursion(_node, _is_container_wrap(_node), _parent);
            }else{
				_node =$(_node).clone()[0];
				if ($M.isMark(_node, "MARK_START")){
						if (!_is_mark_ignore(_node)){
								var _series = $M.getMarkClassSeries(_node) 
								if (_add_wrap) _node = $("<" + _add_wrap +">").append(_node)[0];
								_node_wrapper = $("<div>").attr("id", _series).addClass($M.getMarkCategory(_node)).append(_node)
								$(_parent).append(_node_wrapper)
						}
                }
                else{
						if ($M.isMark(_node)) {
								if (!_is_mark_ignore(_node)){
										var _series = $M.getMarkClassSeries(_node);
										var _p = $(_parent).find("div#" + _series)[0] || (((_container.tag == "pre") || (_container.tag=='code') )&&$(_total).find("div#" + _series)[0])
										$(_p || _parent).append(_node) 
								}
                    }
                    else{
                        $(_parent).append(_node)
                    }
                }
                //if ($M.isMark(_node, "MARK_END")) $(_parent).append("<br />")
            }
        } 
        return _parent;
    };
    _tmp.append(_recursion(_nodes, false, document.createElement("div")))
    //$(_tmp).find(".remarkalbe-end").after("<br />")
    $R.matchAffiliate(_tmp)
	console.log("============");
	//console.log(_tmp.html());
	//console.log($(_tmp).find(":empty").remove());
    return _tmp.html()
}
$R.matchAffiliate = function($body){
    var attr = $R.constant["ATTR_AFFILIATE"]
    var master_slave = {}, ids = []
    $body.find("[" + attr + "]").each(function(){
        var m = $(this).attr(attr);
        if (ids.indexOf(m) < 0) {
            ids.push(m)
            master_slave[m] = []
        }
        var p = $(this).closest("[id^='rmkb-']");
        if ($(this).attr($M.constant["MARK_ATTRIBUTE"]).indexOf(p.attr("id")) >= 0){
            master_slave[m].push(p)
        }
        //var master = $(this).attr(attr)
        //$body.find("[" + $R.constant["ATTR_MARK"] + "=" + "'" + master + "'" + "]").after(this).after("<br />")
    })

    //for lonely topics
    $body.find($M.selectors.byStyle("STYLE_TOPIC")).each(function(){
        var m = $M.getMarkClassSeries(this);
        if (ids.indexOf(m) < 0) {
            ids.push(m);
            master_slave[m] = []
        }
    });
	ids.sort(function(a ,b){
		return parseInt(/\d+/.exec(a)[0]) - parseInt(/\d+/.exec(b))
	})
    var sort_slaves = function(a, b){
        //larger index come first.
        var types = ["STYLE_TOP", "STYLE_TOPIC_WHAT","STYLE_TOPIC_WHY", "STYLE_TOPIC_HOW", "STYLE_TOPIC_GOOD", "STYLE_TOPIC_BAD", "STYLE_TOPIC_EXAMPLE", "STYLE_HIGHLIGHT"].reverse();
        var list = []
        for (var i in types){
            list.push($M.getStyle(types[i]))
        }
        var c = [a, b]
        for (var i in c){
            c[i].mark = c[i].children()[0]
            c[i].mark_type =  $M.getMarkType(c[i].mark);   //$(c[i].mark).attr($M.constant["ATTR_MARK_TYPE"])
            c[i].score = list.indexOf(c[i].mark_type);
            c[i].mark_id = $M.getMarkClassSeries(c[i].mark)
        }
        return (c[1].score - c[0].score) || (c[0].mark_id - c[1].mark_id) 

    }
    $.each(ids, function(){
        var id = this,
            master = $body.find("#" + id),
            slaves = master_slave[id].sort(sort_slaves);
        if (master.length == 0){
            console.log("not find parent for" + id)
            return
        }
		var $ul = $("<ul style='margin:0;padding:0;list-style:none;'>")
        master.wrap("<div style='margin:0;padding:0;'>").wrap("<div>")
        var flag_type = {}
        $.each(slaves, function(){
            var mark = $(this).children()[0]
            var type = $M.getMarkType(mark);//$(mark).attr($M.constant["ATTR_MARK_TYPE"]);
            if ($M.isMark(mark, "STYLE_STRUCTURE")){
                var tag =$(this).find("i.remarkalbe-mark-tag");
				tag.attr("style", $R.styles["mark_tag_plain"])
                if (!flag_type[type]){
                    var h = $("<ol style='margin:0;padding:0';list-style:none;>").attr("class", "remarkalbe-type-description").attr("for", type).append($R.markDescription[tag.html()]);
					$ul.append($("<li style='margin:3px 0;margin-left:3em;'>").append(h))
                    flag_type[type] = 1
                }
                //tag.html((flag_type[type]++).toString() + ".  ")
				tag.remove()
				var header = $ul.find("[for='" + type + "']")[0] || $ul;
				$(header).append($("<li style='margin:3px 0;margin-left:3em;'>").attr("class", type).append(this))
				return 
            }
            $ul.append($("<li style='margin:3px 0;margin-left:3em;'>").attr("class", type).append(this))
        })
		master.parent().append($ul)

    });
	$body.find("p:empty, div:empty").remove();
	//for word
	/*
    var $word_area = $("<div class='remarkalbe-word-area'><h3 class='remarkalbe-visible-head'>Word</h3></div>")
    var c = $M.getStyle("STYLE_WORD")
    $body.find("." + c).each(function(){
        var m = this;
        var parent = $(m).parent();
        if ($M.getNodeNearestMark(m, false)){
            $word_area.append($("<div>").append($(m).clone()));
        }
        else{
            $word_area.append(parent);
        }
        
    });
    if ($word_area.children().length > 1) $body.append($word_area);
	*/
}
$R.markDescription = {
    "W" : "<b>What</b> is it",
    "H" : "<b>How</b> to do",
    "Y" : "<b>Why</b>",
    "G" : "<b>Good</b> parts",
    "B" : "<b>Bad</b> parts",
    "E" : "<b>Example</b>"
}
$R.buildViewNavigation = function(){
    var id = $R.constant["ID_VIEWNAV"]
    var html = "<div id='" + id + "'>"
        + "<ul class='remarkalbe-button-group'>"
        + '<li class="current"><a href="javascript:void()" class="nav-button" title="Full(1)" ><i class="icon-full-view" ></i></a></li>'
        + '<li><a href="javascript:void()" class="nav-button" title="Outline(2)" ><i class="icon-outline-view" ></i></a></li>'
        + '<li><a href="javascript:void()" class="nav-button" title="Simple(3)" ><i  class="icon-keypoint-view" ></i></a></li>'
        + "</ul>"
        + "</div>"
    $(document.body).append(html);
    var t = 'current';
    $("#" + id).on("click", "li", function(){
        $(this).addClass(t);
        $(this).siblings().removeClass(t);
        $R.changeView($(this).index() + 1)
    });
}
$R.toggleColorful = function(flag){
    if (flag){
        $(document.body).addClass($M.getStyle("STYLE_COLORFUL"))
    }else
    {
        $(document.body).removeClass($M.getStyle("STYLE_COLORFUL"))
    }
}
$R.extractMarks = function(_body, type){
		var _result = [];
		//var _content = $("<div>").append(_body);
		$(_body).find($M.selectors.byStyle(type, true)).each(function(){
				var _mark = this, _series = $M.getMarkSeriesByPart(_mark, _body);
				_result.push($R.getPlainText(_series.text()));

		});
		return _result;
}
$R.buildView = function(page,_marked){
	$R.originWindow = page;
	$R.originWindow.postMessage = window.postMessage;
	var _result = $R.analyzePage(page, _marked), 
		_content = _result["content"], 
		_title = _result["title"], 
		_tags=$R.extractMarks(page.document.body, "STYLE_TAG"), 
		_words = $R.extractMarks(page.document.body, "STYLE_WORD");
	var _concat_view = function(_id, _t, _c){
		return 	$(["<div id='", _id ,"'>", 
			$R.generateTitleHeader(_t),
			$R.generateTagSection(_tags),
			$R.generateContentSection(_c),
		   	$R.generateWordSection(_words),	
			$R.generateSourceURLSection(), 
			"</div>"].join(""))	
	};
	var _full = _concat_view("remarkalbe-view-full", _title, _content); //$("<div id='remarkalbe-view-full'></div>").append(_content);
	var _skeleton =  _concat_view("remarkalbe-view-skeleton", _title, $R.getSkeleton($("<div></div>").append(_content)[0]));//$("<div id='remarkalbe-view-skeleton'></div>").append($R.getSkeleton($("<html>").append(_content)[0]))
	var _simple = $("<div id='remarkalbe-view-simple'></div>") 
	var _main = $("<div id='remarkalbe-views'></div>").append(_full).append(_skeleton).append(_simple);

	$R.recommendView = $R.determineView(_skeleton);
	console.log(_tags);
	console.log(_words);
	console.log($R.recommendView);
    $R.buildViewNavigation()
	$R.registerViewChange()
    //$R.toggleColorful(true)
	return _main[0];
}
$R.currentView ="remarkalbe-view-full";
$R.changeView = function(view){
	var _key_event = {1 : "remarkalbe-view-full", 2 : "remarkalbe-view-skeleton", 3 : "remarkalbe-view-simple"};
    var code = parseInt(view)
    switch (code){
		case 2:
		var _content = $("#remarkalbe-view-full .remarkalbe-content-section").html();
		var _container = $("<div></div>").append(_content)[0];
		var _new  = $R.getSkeleton(_container);
		$("#" + _key_event[code] + " .remarkalbe-content-section").html(_new);

        break;
		case 3: 
		$("#" + _key_event[code]).html($("#remarkalbe-view-skeleton").html());
		$(".remarkalbe-content-section", "#" + _key_event[code]).find("h1,h2,h3,h4,h5,h6").remove();
		//$(".remarkalbe-title-header", "#" + _key_event[code]).hide();
        break;
    }
	$R.toggleColorful(code == 1);
	$R.currentView = _key_event[code];
	$("#remarkalbe-views>div").hide();
	$("#" + _key_event[code]).show();
}
$R.clickToChangeView = function(code){
        $("#" +  $R.constant["ID_VIEWNAV"]+ " li").eq(parseInt(code)-1).click()
}
$R.registerViewChange = function(){
	var _key_event = {"1" : "remarkalbe-view-full", "2" : "remarkalbe-view-skeleton", "3" : "remarkalbe-view-simple"};
	document.body.addEventListener('keypress', function(e){
        var code = String.fromCharCode(e.which);
		$R.clickToChangeView(code);
	});
	$(document.body).ready(function(){
		$("#" + _key_event["2"]).hide();
	});
	
}
$R.findMarkInNode = function(_node){
		var _mark_selector = $R.constant["MARK_SELECTOR"];
		var _result = []
		$(_node).find(_mark_selector).each(function(){ 
				var _t = this;// $(this).clone()[0]
				if ($M.isMark(_t , "STYLE_TAG")  || $M.isMark(_t, "STYLE_WORD")) {
						return;
				}
				_result.push($R.purifyContent(_t));
		})
		return _result.join("")
}
$R.getMarksNotIncluded = function(_left, _right){
	var _current_node = _left,
		_above_html = "",
		_below_html = "";
	do{
		if (_current_node.tagName && _current_node.tagName.toLowerCase() == "body") break;
		if (_current_node.previousSibling)
		{
				_current_node = _current_node.previousSibling;
		}else{
				_current_node = _current_node.parentNode;
				continue;
		}
		_above_html = $R.findMarkInNode(_current_node) + _above_html;
	} while (true)
	
	_current_node = _right
	do {
		if (_current_node.tagName && _current_node.tagName.toLowerCase() == "body") break;
		if (_current_node.nextSibling){
			_current_node = _current_node.nextSibling;
		}else{
			_current_node = _current_node.parentNode;
			continue;
		}
		_below_html = _below_html + $R.findMarkInNode(_current_node)
	} while (true)  
	return {above: _above_html, below : _below_html}
}
$R.getFirstMain = function(_candidates){
	var _field = "ratio_length_plain_text_to_total_plain_text",
		_get_ratio = function(a,b){
		return a["assessment"]["text"][_field] / b["assessment"]["text"][_field]
	}

	_candidates.sort($R.commonHelper.candidateSort(["assessment", "text", _field], true, ["index"], false));
	for (var i = 1; i < _candidates.length; i++) {
		var _ratio = _get_ratio(_candidates[i], _candidates[i - 1])
		if (_ratio <= 0.7) return _candidates[i - 1];
	};
	return _candidates[0];

}
$R.determineView = function(_content){
		if ($M.is_pdf){
			return "2";
		}
		// the mark counted should be independent
		var _mark_structures = $(_content).find("div.STYLE_STRUCTURE"),
			_mark_keypoints = $(_content).find("div.STYLE_KEYPOINT"), 
			_mark_keypoints_homeless = $.grep(_mark_keypoints, $M.isHomelessMark),
			_count_heads = $(_content).find("h1, h2, h3, h4, h5, h6").length,
			_count_mark_structures = _mark_structures.length,
			_count_mark_keypoits = _mark_keypoints.length,
			_count_mark_1 = _count_mark_structures + _count_mark_keypoits,
			_count_mark_keypoits_homeless = _mark_keypoints_homeless.length;

		// mark view
		// for mainly utility marks.
		switch(true){
				case  _count_mark_1 <= 5:
			   		return "3";	
		}	

		//outline view
		switch(true){
				case _count_mark_structures >= _count_mark_1 * 0.4 &&  _count_mark_1>= 6:    // many structure marks
				case _count_mark_1 >= 6 && (_count_mark_keypoits_homeless * 2 <= _count_mark_keypoits):   //
				case _count_mark_1 >= 4 && _count_heads >= 3 && _count_heads >= _count_mark_1 * 0.4 &&(_count_mark_keypoits <= _count_heads * 3 ):   //for scenior that has many heads and only highlight seems ok.
					return "2";
		}
		return "1"
}

$R.analyzePage = function (page, _marked){
    var _stuff = $R.getMainContent(page.document.body, _marked),
        //_content = $R.getTitleAndConcatContent(_stuff.candidates[0], _stuff.content, page);
		_result = $R.getTitleAndSplit(_stuff.candidates[0], _stuff.content, page),
		_title = _result.title,
		_content = _result.content,
		_before_node = _result.before_node,
		_after_node = _stuff.candidates[0].node,
		_before_marks = "",
		_after_marks = "";
	if (_marked && _stuff.candidates[0].assessment.marks.ratio_count_marks_to_total_marks == 1){
		console.log("totally");
	}else{
		var _t = $R.getMarksNotIncluded(_before_node, _after_node)
		_before_marks = _t.above
		_after_marks = _t.below
		//_content = $R.addMarkNotIncluded(_content, page.document.body);
	}
	_content = [_before_marks, _content, _after_marks].join("")
    //_content = $R.addMarkTag(_content);
	return { content: _content, title: _title}
}
var _gaq = _gaq || [];                                               
_gaq.push(['_setAccount', 'UA-36157260-2']);                         
_gaq.push(['_trackPageview', window.location.href]);                                       

(function() {                                                        
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;                                                      
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';                             
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
