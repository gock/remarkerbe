$M = {}
$M.version = "0.4.0"
$M.environment = "development"
$M.env = {
    production :{
        main : "http://remarker.be/",
        sync_service : "http://service.remarker.be/notes/sync"
    },
    // {test}
    test : {
        main : "http://1.remarkerbetest.sinaapp.com/",
        sync_service : "http://service.remarker.be/notes/sync"
    },
    // {/test}
    development: {
        main : "",
        sync_service : "http://0.0.0.0:3000/notes/sync"
    }
}
$M.main = $M.env[$M.environment].main
$M.sync_service = $M.env[$M.environment].sync_service
$M.site = $M.main
$M.main = window.$M_main || $M.main //for plugins
$M.is_pdf = window.$M_is_pdf || ($('script[src*="viewer"]').length && $('script[src*="pdf.js"]').length && $('#viewerContainer').length);
$M.is_extension = (window.$M_main != undefined);
window.$M = $M;
$M.utils = {};
$M.log = {}
$M.counter = 0;
$M.support_save_opener = true;
$M.stack = new function(){
		var _stack = {};
		this.pop = function(cat){
				if (_stack[cat]) return _stack[cat].pop();
		}		
		this.push = function(cat, value){
            if (!_stack[cat]) _stack[cat] = [];
			_stack[cat].push(value);
		}
		this.top = function(cat){
			if (_stack[cat]) return _stack[cat][_stack[cat].length - 1];
		}
}();
$M.log.info = function(msg){
    console.log(msg)
}
$M.saved = true;
$M.assets = {
	"markalbe.js" : "markalbe.js",
	"markalbe.css" : "markalbe.scss.css",
	"markalbe-ui.css" : "markalbe-ui.scss.css",
	"remarkalbe.js" : "remarkalbe.js",
    "jQuery.js" : "jQuery.js",
    "notification.js" : "notification.js"
}
if ($M.environment != "development"){
	$M.assets = {
	"markalbe.js" : $M.main + "js/markalbe.js",
	"markalbe.css" :$M.main + "css/markalbe.scss.css",
	"markalbe-ui.css" : $M.main + "css/markalbe-ui.scss.css",
	"remarkalbe.js" : $M.main + "js/remarkalbe.js",
    "notification.js" :($M.environment=="test")? $M.env['test'].main + "js/notification.js" : $M.env["production"].main + "js/notification.js",
    "jQuery.js" : ($M.is_extension && ($M.main + "js/jquery-1.8.2.min.js")) || "http://code.jquery.com/jquery-1.8.2.min.js" 
	}
}
$M.forDevelop = function(file){
		if ($M.environment == "development"){
				return file + "?" + (new Date()).getUTCMilliseconds()
		}
		return file
}
$M.constant = {
    "APP_NAME" : "remarkalbe",
	"MARK_ATTRIBUTE" : "remarkalbe",
    "STYLE_PREFIX" : "remarkalbe-", 
    "STYLE_HIGHLIGHT" : "highlight",
    "STYLE_BOLD" : "bold",
    "STYLE_UNDERLINE" : "underline",
    "STYLE_ITALIC" : "italic",
    "STYLE_RESTORE" : "restore",
    "STYLE_A" : "answer",
    "STYLE_Q" : "question",
	"STYLE_STEP" : "step",
    "STYLE_NAMING" : "naming",
    "STYLE_PROBLEM" : "problem",
	"STYLE_WORD" : "word",
    "STYLE_TAG" : "tag",
    "MARK_START" : "start",
    "MARK_END" : "end",
    "WRAP_TAG" : "span",
    
    "STYLE_TOPIC" : "topic",
    "STYLE_TOPIC_WHAT" : "topic-what",
    "STYLE_TOPIC_HOW" : "topic-how",
    "STYLE_TOPIC_WHY" : "topic-why",
    "STYLE_TOPIC_GOOD" : "topic-good",
    "STYLE_TOPIC_BAD" : "topic-bad",
    "STYLE_TOPIC_EXAMPLE" : "topic-example",
	"STYLE_CURRENT" : "current",

	"STYLE_KEYPOINT" : "key-point",
	"STYLE_STRUCTURE" : "structure",
	"STYLE_UTILITY" : "utility",
	"STYLE_UTILITY_KEYPOINT" : "utility_keypoint",
	"STYLE_UTILITY_STRUCTURE" : "utility_structure",

	"STYLE_COLORFUL" : "colorful",
	
	"MARK_CID_PREFIX" : "rmkb-",
	
	"ID_READABLE_IFRAME" : "remarkalbe-readable-iframe",
	"TAG_ORIGINAL" : "remarkalbe-original",
	"ID_ORIGINAL_STYLE" : "remarkalbe-readable-original-style",
	"ID_READABLE_STYLE" : "remarkalbe-readable-iframe-style",

	"ID_TOPBAR" : "remarkalbe-topbar",
	
	"HASH_READABLE" : "remarkalbe-readable",

	"ATTR_AFFILIATE" : "remarkalbe-affiliate-for",
	"ATTR_MARK_TYPE" : "remarkalbe-mark-type",
	"HTML_SAVE_FORM" : "<form id='remarkalbe-save-form' action='" + $M.sync_service + "' method='POST' target='_blank' style='display:none !important;'><textarea id='remarkalbe-save-content' type='hidden' name='content'></textarea><input type='hidden' name='title' id='remarkalbe-save-title' /><input type='hidden' name='url' id='remarkalbe-save-url' /> <input type='submit' /></form>",
    "HTML_SAVE_FORM_FOR_AJAX" : "<form id='remarkalbe-save-form' action='" + $M.sync_service + "' method='POST' target='remarkalbe-save-form-result' style='display:none !important;'><textarea id='remarkalbe-save-content' type='hidden' name='content'></textarea><input type='hidden' name='title' id='remarkalbe-save-title' /><input type='hidden' name='url' id='remarkalbe-save-url' /> <input type='submit' /></form><iframe id='remarkalbe-save-form-result' name='remarkalbe-save-form-result'></iframe>",
    "HTML_NOTIFY" : "<div id='remarkalbe-notify'></div>",
    "HTML_MESSAGEBOX" : "<div class='remarkalbe-modal-bg'></div><div class='remarkalbe-info-box'><h3 class='remarkalbe-info-box-title'></h3><p class='remarkalbe-info-box-content'></p><p class='remarkalbe-info-box-buttons'></p></div>"

}
$M.utils.loadJS = function(src, success){
    //$.getScript(src)
    var domScript = $M.utils.createElement('script', { 'src' : src, 'type' : 'text/javascript'});
    success = success || function(){};
    domScript.onload = domScript.onreadystatechange = function() {
        if (!this.readyState || 'loaded' === this.readyState || 'complete' === this.readyState) {
            success();
            this.onload = this.onreadystatechange = null;
            this.parentNode.removeChild(this);
        }
    }
    document.body.appendChild(domScript);
} 
$M.utils.loadCSS = function(src, no_cache){
    if ($M.environment == "development" && no_cache){
        src +="?" + (new Date()).getUTCMilliseconds();
    }
    document.body.appendChild($M.utils.createElement("link", { "href" : src, "type" : "text/css", "rel": "stylesheet"}))
}
$M.utils.createElement = function (tag,attrs){
    var elem = document.createElement(tag);
    for (var key in attrs){
        $(elem).attr(key, attrs[key])
    }
    return elem;
}
$M.utils.invalidNode = function (elem){
    return elem && elem.nodeType != 1;
}
$M.utils.next = function(elem){
    do{
        elem = elem.nextSibling;
    }while(invalidNode(elem));
    return elem;
}
$M.utils.first = function(elem){
    elem = elem.firstChild;
    return invalidNode(elem) ? $M.utils.next(elem) : elem;
}
$M.utils.format = function(str, params){
	for (var i in params) {
		var tag = "{%"+ i + "%}";
		while (str.indexOf(tag) >= 0) str = str.replace(tag, params[i])
	}
	return str
}
$M.downUntilTextNode = function (node) {
    while (node.childNodes.length>0){
        node = node.childNodes[0]; 
    }
    return node;
}
$M.getSelectionContent = function(){
    var s = window.getSelection(),
        r = s.getRangeAt(0),
        content = s.toString();
    if (content.trim() == ""){
        var fragment = r.cloneContents();
        content = $.map(fragment.childNodes, function(item){ return item.outerHTML }).join("");
    }
    return content.toString();
}
$M.get_ends = function (s){
	var anchorNode = s.anchorNode
    var focusNode = s.focusNode
    var anchorOffset = s.anchorOffset
    var focusOffset = s.focusOffset
	//var direction = s.containsNode() 
	var scale_selection = function(node, loc, direction){
			if (node.nodeValue && node.nodeValue[loc + 1 * direction] !=" " && anchorNode.nodeValue[loc + 2 * direction] == " ") {
					return 1 * direction	
					}else{
							return 0
					}
			}

	if (anchorNode == focusNode)
	{
        if (anchorNode.splitText){
            var a = s.focusOffset
            var b = s.anchorOffset
            if (a > b)
            {
                anchorOffset += scale_selection(anchorNode, b, -1)
                focusOffset += scale_selection(focusNode, a - 1, 1)
                anchorNode.splitText(anchorOffset)
            }
            else
            {
                anchorOffset += scale_selection(anchorNode, b - 1, 1)
                focusOffset += scale_selection(focusNode, a, -1)
                anchorNode.splitText(focusOffset)

            }
            elem = anchorNode.nextSibling
            elem.splitText(Math.abs(focusOffset - anchorOffset))
            return [anchorNode.nextSibling, anchorNode.nextSibling]
        }
        else
        {
            var a  = anchorOffset, b= focusOffset, _from = Math.min(a, b), _to = Math.max(a ,b);
            return [anchorNode.childNodes[_from], anchorNode.childNodes[_to - 1]];
        }
    }
	s.containsNode(anchorNode, false) && anchorNode.splitText && anchorNode.splitText(anchorOffset)
    s.containsNode(focusNode, false) && focusNode.splitText && focusNode.splitText(focusOffset)
    if (!focusNode.splitText && focusNode.childNodes){
        focusNode = focusNode.childNodes[focusOffset - 1];
    }
    if (!anchorNode.splitText && anchorNode.childNodes){
        anchorNode = anchorNode.childNodes[anchorOffset - 1];
    }
	if (s.containsNode(anchorNode.nextSibling, false))
	{
		start = anchorNode.nextSibling
		end = focusNode
	}
	else
	{
		end = anchorNode
		start = focusNode.nextSibling
	}
	return [start, end]
}
$M.isCombo = function(){
    return $M.getSelectionContent() == $M.lastRecord.content
}
$M.lastRecord = {}
$M.saveRecord = function(nodes){
       if ($M.isCombo()) return;
       $M.lastRecord.start = nodes[0].parentNode;
       $M.lastRecord.end = nodes[nodes.length - 1].parentNode;
       $M.lastRecord.content = $M.lastContent;
}
$M.find_selection_nodes = function(){
	var s = window.getSelection()
	var r = s.getRangeAt(0)
    
	var stack = []
    var isCombo = $M.isCombo();
	var ends = isCombo? [$M.lastRecord.start, $M.lastRecord.end] : $M.get_ends(s)
	var current = ends[0]
	while (current && current != ends[1]) {
		if (s.containsNode(current, false))
		{
            if (current != ""){
                stack.push(current)
            }
			while (current.nextSibling == null)
			{
				current = current.parentNode
			}
			current = current.nextSibling
		}
		else
		{
			if (s.containsNode(current, true))
			{
				current = current.firstChild
			}else
			{
				break;
			}
		}
	}
	if (current == ends[1]) stack.push(ends[1]);	
    //in combo, the EM ends are not totally contained in selection
    if (isCombo){
        stack.shift(0);
        stack.unshift(ends[0]);
    }   
	//alert(stack)
	return stack
}
$M.selectors = {
    byStyle: function(s, start){ return "." + $M.getStyle(s) + (start? $M.selectors.byStyle("MARK_START") : "")},
	byAttr: function(id, operator){
			var s = ["[", $M.constant["MARK_ATTRIBUTE"]];
			if (id){
					operator = operator || "="
					s.push(operator, "'", id, "'")
			}
			s.push("]")
			return s.join("")
        },
    byType: function(s, start){
        var s = ["[",$M.constant['ATTR_MARK_TYPE'], '=',s ,"]"];
        if (start) s.unshift($M.selectors.byStyle("MARK_START"));
        return s.join("");
    }

}
$M.isHomelessMark = function(_node){
    return !$(_node).attr($M.constant["ATTR_AFFILIATE"]);
}

$M.generateMarkClassId = function(count){
    return $M.constant['MARK_CID_PREFIX'] + $M.counter + '_' + count;
}
$M.getMarkClassSeries = function(node){
	var attr = $(node).attr($M.constant["MARK_ATTRIBUTE"])
    if (!attr) return null;
    var i = attr.lastIndexOf("_");
	return attr && i>=0 && attr.substring(0, i + 1);
}
$M.getMarkSeriesByPart = function(node, _body){
	var attr = $M.getMarkClassSeries(node);
	return $M.getMarkSeries(attr, _body)

}
$M.getMarkType = function(node){
	return $(node).attr($M.constant["ATTR_MARK_TYPE"]) || $($M.getMarkSeriesByPart(node)[0]).attr($M.constant["ATTR_MARK_TYPE"])
}
$M.processSelection  = function(nodes, type){
	var _count = 0
	var result = []
	for (var i = 0; i < nodes.length; i++) {
        var $node = $(nodes[i])
        if ($M.isMark(nodes[i])){
			$node.addClass(type);
			result.push(nodes[i])	

            $node.attr($M.constant["MARK_ATTRIBUTE"], $M.generateMarkClassId(_count));
        }else{
			var attr = { "class":type};
			attr[$M.constant["MARK_ATTRIBUTE"]] = $M.generateMarkClassId(_count);
            $node.wrap($M.utils.createElement($M.constant["WRAP_TAG"], attr));
			result.push($M.getNodeNearestMark(nodes[i]));
        }
		_count ++;
       
	}
    $M.stack.push(type, $M.generateMarkClassId(""));

	var start = $M.getNodeNearestMark(nodes[0]),
		end = $M.getNodeNearestMark(nodes[nodes.length - 1]);
	$(start).addClass($M.getStyle("MARK_START"));
	$(end).addClass($M.getStyle("MARK_END")); 
	//$(start).attr($M.constant["ATTR_MARK_TYPE"], type)
	$(result).each(function(){ $(this).attr($M.constant["ATTR_MARK_TYPE"], type); } )
	$M.triggerProcessCallback(type, start, end,  result)		
    
    $M.stack.push("ALL", $M.generateMarkClassId(""));
}
$M.registerProcessCallback = function(type, callback){
	//console.log(callback)	
	$M._process_callback = $M._process_callback || {}
	$M._process_callback[type] = $M._process_callback[type] || []
	$M._process_callback[type].push(callback)
}
$M.triggerProcessCallback = function(type, start,end, nodes){
		if 	($M._process_callback && $M._process_callback[type]){
		}else{
			return;
		}
		for (var i = 0; i < $M._process_callback[type].length; i++) {
			$M._process_callback[type][i](start, end, nodes)
		};		
}
$M.isMark = function(node, name){
    
    if (node && node.tagName && node.tagName.toLowerCase() == $M.constant["WRAP_TAG"] && node.className.indexOf($M.constant['STYLE_PREFIX']) > -1 ){
        if (name == undefined){
            return true;
        }else{
            if ($(node).hasClass($M.getStyle(name))) return true;
        }
    }
    return false;
        
}
$M.getMarkCategory = function(_node){
    var result = ""
    $(["STYLE_STRUCTURE", "STYLE_KEYPOINT", "STYLE_UTILITY"]).each(function(){
        if ($M.isMark(_node, this)) {
            result = this.toString();
            return false;
        }
    })
    return result;
}
$M.getFocusedMark = function(){
    var s = window.getSelection();
    var start = s.anchorNode;
    return $M.getNodeNearestMark(start)
}
$M.getNodeNearestMark = function(start, self){
	var node;
	if (self == undefined) self = true;
    if ($M.isMark(start) && self){
        node = start
    }else{
        $(start).parents($M.constant["WRAP_TAG"]).each(function(){
            if ($M.isMark(this)){
                node = this;
                return false;
            }
        });
    }
    return node
}
$M.selectRange = function(start, end){
	var range = document.createRange();
	var s = window.getSelection();
	if (!end){
		range.selectNode(start);
	}else{
		range.setStart(start);
		range.setEndAfter(end);
	}
	s.removeAllRanges();
	s.addRange(range)
}
$M.selectMark = function(node){
	var n = $M.getNodeNearestMark(node)
	if (!n) return
	var nodes = $M.getMarkSeriesByPart(node)
	$M.selectRange(nodes[0], nodes[nodes.length - 1])
}
$M.getMarkByMarkAttribute = function(id){
	return $($M.selectors.byAttr(id))[0] 
}
$M.getMarkSeriesStart = function(s){
	return $M.getMarkByMarkAttribute(s + "0");
}
$M.getMarkSeries = function(q, _body){
	return $($M.selectors.byAttr(q, "^="), _body?_body : document.body);
}
$M.deleteMark = function(){
    var node = $M.getFocusedMark() || $M.lastRecord.start
    if (!node) return;
    if ($(node).is('.' + $M.getStyle("STYLE_CURRENT"))) $M.stack.pop($M.getStyle("STYLE_CURRENT"))
    if ($(node).is('.' + $M.getStyle("STYLE_TOPIC"))) $M.stack.pop($M.getStyle("STYLE_TOPIC"))
    if ($(node).is('.' + $M.getStyle("STYLE_Q"))) $M.stack.pop($M.getStyle("STYLE_Q"))
   	$M.getMarkSeriesByPart(node).each(function(i){
        var $node = $(this);
        $node.mouseleave();
        $node.after($node.html());
        this.parentNode.removeChild(this);
    });
    $M.lastRecord = null;
}
$M.existHeaderBetween = function(_current_node){
    var _common = $(_current_node).closest(":has(.remarkalbe-current)")[0];
    var _c = $(".remarkalbe-current")[0];
    var _t = _current_node;
    do{
        if (_current_node.tagName && _current_node == _common) break;   // find no head
        if (_current_node == _c) return false;
        if (_current_node.previousSibling)
        {
            _current_node = _current_node.previousSibling;
        }else{
            _current_node = _current_node.parentNode;
            continue;
        }
        if ($(_current_node).has(_c)[0]) return false;
        if ($(_current_node).is(":header")
            || $(_current_node).has(":header")[0])
            return true;
    } while (true)
    _current_node = _c;
    do {
        if (_current_node.tagName && _current_node == _common) break;
        if (_current_node.nextSibling){
            _current_node = _current_node.nextSibling;
        }else{
            _current_node = _current_node.parentNode;
            continue;
        }
        if ($(_current_node).is(":header")
        || $(_current_node).has(":header")[0])
        return true;
    } while (true)  
    return false;
}
$M.refreshCurrent = function(node){
	if (node.length) node = node[0]
	var c = $M.getStyle("STYLE_CURRENT");
	if ($(node).hasClass(c)){
		$(node).removeClass(c);
        $M.stack.pop(c)
        return false;
	}else{
		var before = $M.stack.pop(c)
		$($M.getMarkSeriesStart(before)).removeClass(c)
		$(node).addClass(c);
		$M.stack.push(c, $M.getMarkClassSeries(node))		
        return $M.getMarkClassSeries(node);
	}
}
$M.getMaster = function(slave){
		var start = $M.getMarkSeriesByPart(slave)[0];
		var q = $(start).attr($M.constant["ATTR_AFFILIATE"])
		return q && $M.getMarkSeries(q);	
}
$M.getStyle = function(style){
    return $M.constant["STYLE_PREFIX"] + $M.constant[style]
}
$M.keyEvent = {
    'h' : $M.getStyle("STYLE_HIGHLIGHT"),
    'b' : $M.getStyle("STYLE_BOLD"),
    'u' : $M.getStyle("STYLE_UNDERLINE"),
    'i' : $M.getStyle("STYLE_ITALIC"),
    'z' : $M.getStyle("STYLE_RESTORE"),
    
    'q' : $M.getStyle("STYLE_Q"),
    'a' : $M.getStyle("STYLE_A"),
    
	//'s' : $M.getStyle("STYLE_STEP"),

    'n' : $M.getStyle("STYLE_NAMING"),
	'?' : $M.getStyle("STYLE_PROBLEM"),
	'w' : $M.getStyle("STYLE_WORD"),

    't' : $M.getStyle("STYLE_TAG"),
	
	'T' : $M.getStyle("STYLE_TOPIC"),
	'W' : $M.getStyle("STYLE_TOPIC_WHAT"),
	"H" : $M.getStyle("STYLE_TOPIC_HOW"),
	"Y" : $M.getStyle("STYLE_TOPIC_WHY"),
	"G" : $M.getStyle("STYLE_TOPIC_GOOD"),
	"B" : $M.getStyle("STYLE_TOPIC_BAD"),
	"E" : $M.getStyle("STYLE_TOPIC_EXAMPLE")
}
$M.getTopicStyles = function(){
	var l = ["WHAT", "HOW", "WHY", "GOOD", "BAD", "EXAMPLE"]
	var result = []
	for (var i = 0; i < l.length; i++) {
		result.push($M.getStyle("STYLE_TOPIC_" + l[i]))
	}	
	return result
}
$M.styleTag = function(){
	var dict = {};
	for (var k in $M.keyEvent){
		if (["h", "b", "u", "i", "z", "n", "w", "t"].indexOf(k) < 0){
			dict[$M.keyEvent[k]] = k.toUpperCase();
		}
	}
	return dict
    }();
$M.trigger_press = function(k){
    /*
    var press = $.Event('keypress');

    press.which = key 
    press.bubbles = true;
    press.cancelable = true;
    press.charCode = key;
    press.currentTarget = document
    press.eventPhase = 2;
    press.keyCode = key;
    press.returnValue = true;
    press.srcElement = document;
    press.target = document;
    press.type = "keypress";
    press.view = window;
    $(document).trigger(press);
    */
   var key =  k.charCodeAt(0);
   var keyboardEvent = document.createEvent("KeyboardEvent");
   var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";
   Object.defineProperty(keyboardEvent, 'keyCode', {
                get : function() {
                    return this.keyCodeVal;
                }
    });     
    Object.defineProperty(keyboardEvent, 'which', {
                get : function() {
                    return this.keyCodeVal;
                }
    });     

   keyboardEvent[initMethod](
       "keypress", // event type : keydown, keyup, keypress
       true, // bubbles
       true, // cancelable
       window, // viewArg: should be window
       false, // ctrlKeyArg
       false, // altKeyArg
       false, // shiftKeyArg
       false, // metaKeyArg
       key, // keyCodeArg : unsigned long the virtual key code, else 0
       0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
   );
   keyboardEvent.keyCodeVal = key;
   document.dispatchEvent(keyboardEvent);
    console.log('hererer', k)
}
$M.generateToolbar = function(){
    var html = "";
	var id = $M.constant["ID_TOPBAR"]
	var topbar = $("#" + id)[0] || $("<div id='" + id + "'>")[0]
    html += ''
        + '<ul class="remarkalbe-toolbar">'
		+ '<li><a href="http://remarker.be" id="remarkalbe-logo" target="_blank">R</a></li>'
		//+ '<li  class="remarkalbe-for-origin"><a href="http://remarker.be" id="remarkalbe-logo" target="_blank">R</a></li>'
        + '<li class="remarkalbe-for-readable"><a href="javascript:void()" id="readable-save-to-note" title="Save to Evernote">Save current view</a></li>'
        + '<li class="remarkalbe-for-readable"><a href="javascript:void()" id="readable-back">Back</a></li>'
        + '<li class="remarkalbe-for-origin" id="remarkalbe-save"><div class="remarkalbe-btn-group"><a href="javascript:void(0)" id="remarkalbe-smart-save" title="Save to Evernote">Save</a><button class="remarkalbe-dropdown-toggle remarkalbe-btn"><span class="remarkalbe-caret"></span></button><ul class="remarkalbe-dropdown-menu"><li><a href="javascript:void()"  id="remarkalbe-custom-save">Custom Save...</a></li></ul></li>'
        + '<li class="remarkalbe-for-origin" style="display:none !important;"><a href="#' + $M.constant["HASH_READABLE"] + '" id="remarkalbe-toolbar-export">Export</a></li>'
		+ '<li class="current remarkalbe-for-origin" id="remarkalbe-help"><a href="javascript:void()">Help</a>'
		+ "<div id='remarkalbe-shortcut-help'>"
		+ (function(){
			var list = {
				"Keypoint tools" : { "h" : "highlight", "b" : "bold", "i" : "italic" , "u" : "underline"},
				"Topic tools" : { "T" : "Topic", "W" : "What", "H" : "How", "Y" : "whY", "G" : "Good", "B" : "Bad", "E" : "Example" },
				"Utility tools" : { "q" : "question", "a" : "answer", "t" : "tag", "w" : "word", "z" : "cancel"}
			}
			var result = []
			for (var t in list){
				var pair = list[t]
				result.push("<div class='", t.toLowerCase().replace(/\s/,'-') ,"'>", t, "</div>")
				result.push("<ul>")
				for (var k in pair){
					result.push("<li><kbd class='", 'key-' + k,"'>", k, "</kbd><span>", pair[k], "<span></li>")
				}
				result.push("</ul>")	
			}
			return result.join("")
			}())	
		+ "<a id='remarkalbe-view-doc' href='http://remarker.be/getting_started'>view doc</a>"
		+ "</div>"
		+ '</li>'
        + '</ul>'
	$(topbar).html(html)
    $(document.body).append(topbar);
	$('#remarkalbe-help a').click(function(){
		$(this).parent().toggleClass("current")
    });
    $('kbd', topbar).mousedown(function(event){
        var text = $(this).text();
        $M.trigger_press(text);
    })
    var onresize = function(){
        $(topbar).toggleClass('remarkalbe-for-tablet', $(window).width()<1200)
    }
    $(window).on('resize', onresize);
    onresize();
	/*
    $('#remarkalbe-topbar').css("right", "-160px")
	$('#remarkalbe-topbar').hover(function(){
		$(this).animate({right : "0"})
	},function(){
		setTimeout(function(){
			$('#remarkalbe-topbar').animate({right : "-160px"})
		},5000);
	});
	*/
	$("#remarkalbe-toolbar-export, #remarkalbe-custom-save").click(function(){
		//alert("hello")
		$M.launch_readable()
		_gaq && _gaq.push(["_trackEvent", "click", "Export" ])
		//$M.utils.loadJS("remarkalbe.js");
	});
	$("#readable-back").click(function(){

		_gaq && _gaq.push(["_trackEvent", "click", "Back" ])
        //window.parent.location.hash = "";
        window.parent.$M.backOriginal();
	});
    $("#remarkalbe-smart-save").click(function(){
        var func_save = function(){
            _gaq && _gaq.push(["_trackEvent", "click", "Save" ])
            $M.notify("Analyzing and processing content and marks...")
            $M.launch_invisible_readable();
        }
        if ($M.stack.top($M.getStyle("STYLE_TAG"))){
            func_save();
        }
        else{
            $M.messageBox("Info", "You haven't create a <b>[t]ag</b> mark, which is useful for searching and organizing. Please add some [t]ag marks before saving. <a href='" + $M.site + "examples' target='_blank'>See Examples</a>"
                , [{content: "OK", "class": "rmkb-btn"}])
                .fail(func_save);
        }
    });
    $("#readable-save-to-note").click(function(){
        /*
		var $view = $R && $("#" + $R.currentView);
        if ($view.length == 0) {
            alert("Save Fail! Please  save manually.")
            return
        }
		$("#remarkalbe-save-content").val($("<div>").text($view.find(".remarkalbe-content-section").html()).html())
        $("#remarkalbe-save-title").val($view.find(".remarkalbe-title-header").text())
        $("#remarkalbe-save-url").val($view.find(".remarkalbe-source-url-section a").text())
		$("#remarkalbe-save-form").submit()	
        */
       $M.notify("Saving...")
       $M.completeAndSubmitForm(window)
	})
}
$M.hide_original = function(){
	var html = document.getElementsByTagName('html')[0],
		html_selector = html.id && html.id > '' ? "#" + html.id : "html",
		
		body = document.getElementsByTagName("body")[0],
		body_selector = body.id && body.id > '' ? "#" + body.id : "body",
		cssText = "{%html%}.{%original%}, {%html%} > {%body%}.{%original%}, {%body%}.{%original%} { position: static !important; margin: 0 !important; padding: 0 !important; border: 0 !important; } {%html%}.{%original%}, {%html%} > {%body%}.{%original%}, {%body%}.{%original%} { overflow: hidden !important; overflow-x: hidden !important; overflow-y: hidden !important; } {%html%}.{%original%} object, {%html%}.{%original%} embed, {%html%}.{%original%} iframe, {%html%} > {%body%}.{%original%} object, {%html%} > {%body%}.{%original%} embed, {%html%} > {%body%}.{%original%} iframe, {%body%}.{%original%} object, {%body%}.{%original%} embed, {%body%}.{%original%} iframe { visibility: hidden !important; } {%html%}.{%original%} {%readable-iframe%}, {%html%} > {%body%}.{%original%} {%readable-iframe%}, {%body%}.{%original%} {%readable-iframe%}, {%readable-iframe%} { display: block !important; overflow: auto !important; visibility: visible !important; } ",
		cssElement = $M.utils.createElement("style", {
			"id" : $M.constant["ID_ORIGINAL_STYLE"],
			"type" : "text/css"
		});
	cssText = $M.utils.format(cssText, {
		"html" : html_selector,
		"body" : body_selector,
		"readable-iframe" : $M.constant["ID_READABLE_IFRAME"],
		"original" : $M.constant["TAG_ORIGINAL"]
		});
	$(cssElement).html(cssText);
	document.body.appendChild(cssElement);
	$(html).addClass($M.constant["TAG_ORIGINAL"]);
	$(body).addClass($M.constant["TAG_ORIGINAL"]);
}
$M.lauch = function(){
    //$M.utils.loadCSS('http://1.remarkalbe.sinaapp.com/css/main.css', true)
	$M.utils.loadCSS($M.forDevelop($M.assets['markalbe-ui.css']), true);
    $M.in_readable = (document.body.id == "remarkalbe-readable-body");
	if (!$M.in_readable) {
        $M.utils.loadCSS($M.forDevelop($M.assets['markalbe.css']), true);
        $M.checkSaved();
	}
    $M.generateToolbar();
    $(document.body).addClass($M.getStyle("STYLE_COLORFUL"));
    if ($M.is_pdf) $(document.body).addClass('rmkb-pdf');
    document.addEventListener('keypress', function(e){
        
        var code = String.fromCharCode(e.which);
        var code = String.fromCharCode(e.which);
        console.log(e, e.which);
		if (e.which == 10 && e.ctrlKey == 1){
			window.location = "#" + $M.constant["HASH_READABLE"]
			_gaq && _gaq.push(["_trackEvent", "keyDown", "Export" ])
			//$M.launch_readable()	
			return;
		}
        if (!(code in $M.keyEvent) || ($M.getSelectionContent() == "" && code !='z')){
            return
		}

		_gaq && _gaq.push(["_trackEvent", "keyDown", code])
        if (code != 'z'){
            $M.lastContent = $M.getSelectionContent();
            var nodes = $M.find_selection_nodes();
            $M.counter ++;
            $M.processSelection(nodes, $M.keyEvent[code])
            $M.saveRecord(nodes)
            $M.log.info($M.lastRecord)
        }else{
            $M.deleteMark()
            $M.lastRecord = {}
        }
        $M.saved = false;
	}, false);
	

	$(document.body).on("dblclick",$M.selectors.byAttr(), function(){$M.selectMark(this)})

	$(document.body).on("click",[$M.selectors.byStyle("STYLE_STRUCTURE"), $M.selectors.byStyle("STYLE_A"), $M.selectors.byStyle("STYLE_Q")].join(","), function(){ 
        if (window.getSelection().toString()!="") return;  //if there's selection when click, maybe user wants to delete the mark.
		var start = $M.getMarkSeriesByPart(this)[0];
        var current = $M.refreshCurrent(start)
        if (current){
            $M.stack.push("ALL", current);
            if ($M.isMark(start, "STYLE_TOPIC")) $M.stack.push($M.getStyle("STYLE_TOPIC"), $M.getMarkClassSeries(start));
        }
	})

	var stripe_class = "stripes-darker"		
	$(document.body).on("mouseenter", $M.selectors.byAttr(), function(){
		if ($M.isMark(this, "STYLE_TOPIC")) return
		console.log("in")
		var master = $M.getMaster(this)	
		$(master).addClass(stripe_class)
	})
	$(document.body).on("mouseleave", $M.selectors.byAttr(), function(){$($M.getMaster(this)).removeClass(stripe_class)})
	$M.registerProcessCallback($M.getStyle("STYLE_A"), function(start, end, nodes){
			var q = $M.stack.top($M.getStyle("STYLE_Q"))
			$(end).attr($M.constant["ATTR_AFFILIATE"], q)
	})
	$(["WHAT", "HOW", "WHY", "GOOD", "BAD", "EXAMPLE"]).each(function(){
        $M.registerProcessCallback($M.getStyle("STYLE_TOPIC_" + this), function(start, end, nodes){
					$(nodes).each(function(i){
						$(nodes[i]).addClass($M.getStyle("STYLE_STRUCTURE"))
					})
                    var q = $M.stack.top($M.getStyle("STYLE_TOPIC"))
                    $(start).attr($M.constant["ATTR_AFFILIATE"], q)
                    if (!q){
                        $M.notify("You'd better create a [T]opic mark before creating a W/H/G/B/E mark.", 10000, true, 'alert')
                    }
                    else {
                        $M.dismiss_notify();
                    }
					if (q != $M.stack.top($M.getStyle("STYLE_CURRENT"))) $M.refreshCurrent($($M.selectors.byAttr(q,"^="))[0])
			});
	});
	$(["HIGHLIGHT", "BOLD", "UNDERLINE", "ITALIC"]).each(function(){
			$M.registerProcessCallback($M.getStyle("STYLE_" + this), function(start, end, nodes){
					$(nodes).each(function(i){
						$(nodes[i]).addClass($M.getStyle("STYLE_KEYPOINT"))
					})
					var q = $M.stack.top($M.getStyle("STYLE_CURRENT"))
                    if (q && $M.stack.top("ALL") != q  && $M.existHeaderBetween(start)){ // the last mark is  current, and there's a header betwwen them, then inactive current
                        $M.refreshCurrent($($M.selectors.byAttr(q,"^="))[0])
                        $M.stack.pop($M.getStyle("STYLE_CURRENT"))
                        $M.notify("Inactivated the CURRENT mark. Because it's above the section.", 3*1000);
                    }else{
                        $(start).attr($M.constant["ATTR_AFFILIATE"], q)
                    }
			});
	});
	$(["Q", "A", "STEP", "PROBLEM", 'TAG', 'WORD']).each(function(){
			var tag = this;
			$M.registerProcessCallback($M.getStyle("STYLE_" + tag), function(start, end, nodes){
					$(nodes).each(function(i){
						$(nodes[i]).addClass($M.getStyle("STYLE_UTILITY"))
						//$(nodes[i]).addClass($M.getStyle("STYLE_UTILITY_STRUCTURE"))
					})
					var q = $M.stack.top($M.getStyle("STYLE_CURRENT"))
					if (tag == "A"){
						q = $M.stack.top($M.getStyle("STYLE_Q")) 
					}
					$(start).attr($M.constant["ATTR_AFFILIATE"], q)
			});
	});
	$M.registerProcessCallback($M.getStyle("STYLE_TOPIC"), function(start, end, nodes){
			$(nodes).each(function(i){
					$(nodes[i]).addClass($M.getStyle("STYLE_STRUCTURE"))
			})
			$M.stack.push("*", $M.refreshCurrent(start));
        });
        $M.utils.loadJS($M.forDevelop($M.assets['notification.js']));
        $(document).on("click", ".remarkalbe-dropdown-toggle",function(){
            $(this).toggleClass("active");
            return false;
        });
        $(document).on("click", ".remarkalbe-dropdown-menu",function(){
            $(".remarkalbe-dropdown-toggle").removeClass("active");
        });
        $(document).click(function(e){
            $(".remarkalbe-dropdown-toggle").removeClass("active");
        })

        $M.create_save_form();
        $M.addMessageListener();
};
$M.ready = function(delayedNrTimes){
    if (window.console && window.console.log) {
		window.console.log('launch ready ' + delayedNrTimes);
	}
	//document.readyState != 'complete' ||
	if ((!document.body || $ == undefined) && delayedNrTimes < 30) {
		setTimeout(function () {
			$M.ready (delayedNrTimes + 1);
		}, 100);
		return;
	}
	$M.lauch();

}
$M.loadAssets = function(_assets){
	_result = ""
	_css_template = '<link rel="stylesheet" href="%{src}" type="text/css" media="screen" />'
	_js_template = '<script type="text/javascript" src="%{src}"></script> '
	for (var i in _assets){
		switch (i){
		case "css":
			$(_assets[i]).each(function(j){
				_result += _css_template.replace('%{src}', _assets[i][j])
			})
			break;
		case "js":
			$(_assets[i]).each(function(j){
				_result += _js_template.replace('%{src}', _assets[i][j])
			})
		}
	}
	return _result;
}
$M.NOTIFY_TIMER = ""
$M.notify = function(_html, _time, _clickhide, type){
    var _notify = $("#remarkalbe-notify")[0];
    if (_notify){
        _notify.className = ""
        clearTimeout($M.NOTIFY_TIMER);
        $(_notify).html(_html).show();
        if (_time){
            $M.NOTIFY_TIMER = setTimeout(function(){ $M.dismiss_notify()}, _time);
        }
        if (_clickhide){
            $(_notify).click(function(){ $M.dismiss_notify()});
        }
        if (type){
            $(_notify).addClass(type);
        }
    }else{
        $(document.body).append($M.constant['HTML_NOTIFY']);
        $M.notify(_html, _time, _clickhide, type);
    }
}
$M.dismiss_notify = function(){
    $("#remarkalbe-notify").fadeOut();
    //$("#remarkalbe-notify").remove();
}
$M._dtd = $.Deferred();
$M.messageBox = function(title, content, buttons){
    $M._dtd = $.Deferred();
    var html = $M.constant['HTML_MESSAGEBOX'];
    var $box = $(html);
    $box.find("h3").text(title);
    $box.find(".remarkalbe-info-box-content").html(content);
    $(buttons).each(function(){
        var obj = this;
        $box.find(".remarkalbe-info-box-buttons").append($("<a>").attr("href", obj['href'] || "javascript:void(0)").attr("target", obj['target'] || "_self").attr("type", obj['type']).attr("class", obj['class']).text(obj["content"]));
    })
    $(document.body).append($box);
    $(document.body).on("click", ".remarkalbe-info-box-buttons a, .remarkalbe-modal-bg", function(){
        if ($(this).is("[type=success]")) $M._dtd.resolve();
        if ($(this).is("[type=fail]")) $M._dtd.reject();
        var box = $(".remarkalbe-modal-bg, .remarkalbe-info-box");
        box.hide();
        box.remove();

    })
    return $M._dtd;
    
}
$M.create_save_form = function(){
    var _form_html = "<form id='remarkalbe-save-form' action='" + $M.sync_service + "' method='POST' target='remarkalbe-save-form-result' style='display:none !important;' accept-charset='utf-8'>"
    + "<textarea id='remarkalbe-save-content' type='hidden' name='note[content]'></textarea>"
    + "<input type='hidden' name='note[title]' id='remarkalbe-save-title' />"
    + "<input type='hidden' name='note[url]' id='remarkalbe-save-url' />"
    + "<input type='hidden' name='note[tags]' id='remarkalbe-save-tags'>"
    + "<input type='hidden' name='note[words]' id='remarkalbe-save-words'>"
    + "<input type='submit' /></form>"
    var _iframe = "<iframe id='remarkalbe-save-form-result' name='remarkalbe-save-form-result' style='display:none !important;'></iframe>";
    $(document.body).append(_form_html);
    $(document.body).append(_iframe);

}
$M.create_readable = function(invisible, script){
	var already = $("#" + $M.constant["ID_READABLE_IFRAME"])
	if  (already.length>0){
		already.remove();
	}
	var 
		
		cssText = "width:100% !important;height:100% !important;visibility:visible !important; display:block !important; overflow:auto !important; position:fixed !important; z-index:2147483646 !important; top:0; left:0; background:#fff !important";
    if (invisible){
        cssText = "width:0 !important;height:0 !important;visibility:hidden !important; display:none !important; overflow:auto !important; position:fixed !important; z-index:-1 !important; right:0; bottom:0; background:#fff !important";
    }
	var _iframe = $M.utils.createElement("iframe", {
			'id' : $M.constant["ID_READABLE_IFRAME"],
			'frameBorder' : '0',
			'allowTransparency' : 'true',
			'scrolling' : 'auto',
			'style' : cssText
		})
		_assets = {
			"js" : [$M.assets['jQuery.js'], $M.forDevelop($M.assets["markalbe.js"]), $M.forDevelop($M.assets['remarkalbe.js'])],
			"css" : [$M.forDevelop($M.assets['markalbe-ui.css'])]
		},
		_iframe_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'
			+ '<html id="html" xmlns="http://www.w3.org/1999/xhtml">'
			+ '<head>'
            + '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">'
            + '<script>window.$M_main =' + ((window.$M_main && ['"', window.$M_main, '"'].join(""))) + '</script>'
            + '<script>window.$M_is_pdf =' + $M.is_pdf + '</script>'
			+ $M.loadAssets(_assets)
			+ '</head>'
			+ '<body class="remarkalbe-readable-body" id="remarkalbe-readable-body">'
			+ '<h1 class="loading-head">loading...</h1>'
			+ '<script>$(document.body).append($R.buildView(window.parent, true));$(".loading-head").hide();$M.generateToolbar();</script>'
            + (invisible ? '<script>$R.clickToChangeView($R.recommendView);$R.originWindow.postMessage("ready", "*");</script>': "")
			+ '</body>'
			+ '</html>';
	
		cssText = "{%iframe%}{width:100% !important;height:100% !important;visibility:visible !important; display:block !important; overflow:auto !important; position:fixed !important; z-index:2147483646 !important; top:0; left:0;}",
		cssElement = $M.utils.createElement("style",{
			"id" : $M.constant["ID_READABLE_STYLE"],
			"type" : "text/css"
		});
	cssText = $M.utils.format(cssText, {"iframe" : "#" + $M.constant["ID_READABLE_IFRAME"]});
	$(cssElement).html(cssText);
	document.body.appendChild(cssElement);
   document.body.appendChild(_iframe);	
   var _f = document.getElementById($M.constant["ID_READABLE_IFRAME"]);
	var _doc = (_iframe.contentDocument || _iframe.contentWindow.document);
    _doc.open();
    _doc.write(_iframe_html)
    _doc.close();
    _f.focus();
    return _f 

}
$M.checkSaved = function(){
    window.onbeforeunload = function(){
        if (!$M.saved) return "You have marks not saved. Leaving the page will discard what you mark. Are you sure to exit? ";
    }
}
$M.completeAndSubmitForm = function(_window){
    var $view = _window.$R && $("#" + _window.$R.currentView, _window.document.body);
    if (!$view || $view.length == 0) {
        alert("Save Fail! Please  save manually.")
        return
    }
    var _get_arrary_value = function(_elements){
        var list = $.map(_elements, function(_item){ return $(_item).text();})
        return    JSON.stringify(list);//"[" + $.map(_elements, function(_item){ return "\"" + $(_item).text() + "\""}) + "]";
    }
    $("#remarkalbe-save-content").val($("<div>").text($view.html()).html())
    $("#remarkalbe-save-title").val($view.find(".remarkalbe-title-header").text())
    $("#remarkalbe-save-url").val($view.find(".remarkalbe-source-url-section a").text())
    $("#remarkalbe-save-words").val(_get_arrary_value($view.find(".remarkalbe-word-section .remarkalbe-word-section-word")));
    $("#remarkalbe-save-tags").val(_get_arrary_value($view.find(".remarkalbe-tag-section .remarkalbe-tag-section-tag")));
    $("#remarkalbe-save-form").submit()	
}
$M.addMessageListener = function(){
    var _host_regex = new RegExp("http.*?//[^/]*");
    window.addEventListener('message', function(e){
        console.log(e);
        switch(true){
            case e.origin.indexOf(document.domain) >= 0 && e.data == "ready":
                var _iframe = document.getElementById($M.constant["ID_READABLE_IFRAME"]);
                $M.notify("Saving...");
                $M.completeAndSubmitForm(_iframe.contentWindow);
                break;
            case e.data == 'save':
                $("#remarkalbe-smart-save").click()
                break;
            case e.data  == 'saved':
                $M.notify('Saving...', null, false)
                setTimeout(function(){
                    $M.notify('Save successfully!', null, false)
                }, 3000)
                $M.saved = true;
                break;
            case e.data.status && _host_regex.test($M.sync_service):
                $M.processResponse(e.data);
                break;
        }
    });
}
$M.processResponse = function(data){
    switch (data['status']){
        case "302":
            $M.dismiss_notify();
            $(document.body).append(data['content']);
            break;
        case "200":
            $M.notify("Save successfully!", null, true);
            $M.saved = true;
            if ($M.in_readable){
                window.parent.$M.saved = true;
            }
            break;
        case "500":
            $M.notify(data['content']);

    }
}
$M.launch_invisible_readable = function(){
    $M.create_readable(true);// '$(document).ready(function(){ $("#readable-save-to-note").click() })');
}
$M.launch_readable = function(){
	$M.hide_original();
	console.log("lauch readable")
    $M.create_readable();
}
$M.ready(0);
$M.backOriginal = function(){
	//orginal css
	$("#" + $M.constant["ID_ORIGINAL_STYLE"]).remove()
	$("#" + $M.constant["ID_READABLE_IFRAME"]).remove();
}
function locationHashChanged() {
	console.log(location.hash)
    switch (location.hash) {
	case "":
        console.log("should back")
		$M.backOriginal();
		break;
	case "#" + $M.constant["HASH_READABLE"]:
		var already = $("#" + $M.constant["ID_READABLE_IFRAME"])
		if  (already.length == 0){
			console.log("load readable");
			$M.launch_readable();
		}

		break;
    }
}
 
//window.onhashchange = locationHashChanged;
var _gaq = _gaq || [];                                               
_gaq.push(['_setAccount', 'UA-36157260-2']);                         
_gaq.push(['_trackPageview', window.location.href]);                                       

(function() {                                                        
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;                                                      
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';                             
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
_gaq.push(["_trackEvent", "init", window.location.href])
